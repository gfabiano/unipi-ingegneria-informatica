/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Considerazioni:
 *  Questo esercizio l'ho fatto con lo scopo di divertirmi, perciò è una
 *  soluzione particolare che non tiene conto della complessità spaziale (si
 *  poteva fare ma la voglia non mi assiste) ma allo stesso tempo non pregiudica
 *  la complessità temporale.
 */
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

const int nullptr = 0;

struct Node {
    string val;

    Node *left;
    Node *center;
    Node *right;

    explicit Node(const string &str): val(str) {
        left = center = right = nullptr;
    }
} *root = nullptr;

void Insert(Node *&root, const string &str, bool center = false) {
    string tmp = str;
    if (!center)
        sort(tmp.begin(), tmp.end());

    Node *actual, *prec;
    actual = prec = root;
    while (actual != nullptr) {
        prec = actual;

        if (tmp == actual->val) {
            if (center)
                actual = actual->left;
            else {
                Insert(actual->center, str, true);
                return;
            }
        }
        else if (tmp < actual->val)
            actual = actual->left;
        else
            actual = actual->right;
    }

    Node *NewNode = new Node(tmp);
    if (root == nullptr)
        root = NewNode;
    else if (center && tmp <= prec->val ||
             !center && tmp < prec->val)
        prec->left = NewNode;
    else
        prec->right = NewNode;
   
    if (!center)
        Insert(NewNode->center, str, true);
}

void PrintSubTree(const Node *root) {
    if (root == nullptr) return;
    PrintSubTree(root->left);
    cout << root->val << ' ';
    PrintSubTree(root->right);
}

void PrintTree(const Node *root) {
    if (root == nullptr) return;
    PrintTree(root->left);
    PrintSubTree(root->center);
    cout << endl;
    PrintTree(root->right);
}

int main() {
    int N;
    cin >> N;

    for (int i = 0; i < N; ++i) {
        string tmp;
        cin >> tmp;

        Insert(root, tmp);
    }

    PrintTree(root);

    return 0;
}
