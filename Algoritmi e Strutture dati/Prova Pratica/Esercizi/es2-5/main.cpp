/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Considerazioni:
 *  Ho dedicato alcune ore alla risoluzione di questo esercizio perché secondo
 *  me merita. Non è assolutamente un esercizio difficile, ma ho dedicato del
 *  tempo per trovare una soluzione che mi soddisfacesse dal punto di vista
 *  della complessità.
 *  La prima soluzione a cui avevo pensato consisteva nell'usare array/vector
 *  per contare poi i colori distinti, ma questa soluzione mi ha portato ad
 *  una complessità di O(MN^2) in tutti i casi. Alla fine ho pensato ad una
 *  soluzione 'ibrida', usando array e alberi binari di ricerca portandolo ad
 *  una complessità nel caso medio di O(MNlog(N)) e nel caso peggiore di O(MN^2)
 *
 *  Se avete soluzioni più efficienti di questa vi esorto ad aggiungerla al
 *  repository :)
 */
#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

const int nullptr = 0;

struct PuntoColorato {
    unsigned int x;
    unsigned int y;
    unsigned int c;
};

struct Node {
    unsigned int val;

    Node *left;
    Node *right;

    explicit Node(int unsigned n): val(n) {
        left = right = nullptr;
    }
};

Node **trees;
unsigned int *out;

// Inserisce nell'i-esimo albero il valore n ed incrementa il corrispondente
// risultato su out. Se n è già presente esce dalla funzione e out non viene
// aggiornato.
//
// Complessità:
//      Caso medio: O(log(n))
//      Caso peggiore: O(n)
void Insert(const int i, const unsigned int n) {
    Node *actual, *prec;
    actual = prec = trees[i];
    while (actual != nullptr) {
        prec = actual;

        if (n == actual->val)
            return;
        else if (n < actual->val)
            actual = actual->left;
        else
            actual = actual->right;
    }

    Node *NewNode = new Node(n);
    out[i]++;  // Incrementa il conteggio dei colori distinti della richiesta i

    if (trees[i] == nullptr)
        trees[i] = NewNode;
    else if (n < prec->val)
        prec->left = NewNode;
    else
        prec->right = NewNode;
}

int main() {
    int N, M;
    cin >> N >> M;

    trees = new Node*[M];
    out = new unsigned int[M];

    for (int i = 0; i < M; ++i) {
        trees[i] = nullptr;
        out[i] = 0;
    }

    PuntoColorato *v = new PuntoColorato[N];
    for (int i = 0; i < N; ++i)
        cin >> v[i].x >> v[i].y >> v[i].c;

    // Il numero di iterazioni del blocco più interno, formato solo
    // dall'istruzione Insert(i, v[j].c); , è: M * N
    // Da ciò possiamo calcolare la complessità di questo blocco:
    //      Caso medio: M * N * O(log(N)) = O(NMlog(N))
    //      Caso peggiore: M * N * O(N) = O(MN^2)
    for (int i = 0; i < M; ++i) {
        unsigned int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;

        for (int j = 0; j < N; ++j) {
            if (x1 <= v[j].x && x2 >= v[j].x &&
                y1 <= v[j].y && y2 >= v[j].y) {
                Insert(i, v[j].c);
            }
        }
    }

    for (int i = 0; i < M; ++i) {
        cout << out[i] << endl;
    }

    // La complessità totale del programma:
    //      Caso medio = O(NMlog(N)) + O(N) + 2O(M)
    //      Caso peggiore = O(MN^2) + O(N) + 2O(M)

    // TODO(gfabiano40@gmail.com): Clean trees
    delete[] v;
    delete[] out;

    return 0;
}
