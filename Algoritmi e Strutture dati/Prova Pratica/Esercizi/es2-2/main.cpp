/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Point {
    int x;
    int y;
};

int cmp(const Point& a, const Point& b) {
    if ( a.x == b.x )
        return a.y > b.y;

    return a.x < b.x;
}

int main() {
    int N;
    vector<Point> v;

    cin >> N;

    for (int i = 0; i < N; ++i) {
        Point tmp;

        cin >> tmp.x >> tmp.y;

        v.push_back(tmp);
    }

    sort(v.begin(), v.end(), cmp);

    for (vector<Point>::iterator it = v.begin(); it != v.end(); ++it) {
        cout << it->x << ' ' << it->y << endl;
    }

    return 0;
}
