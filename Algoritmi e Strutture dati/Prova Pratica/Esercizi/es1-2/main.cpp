/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <vector>

using namespace std;

int InsertionSort(vector<int> *vett) {
    int scambi = 0;

    for ( int i = 1; i < vett->size(); ++i ) {
        int mano;
        int occhio;
        mano = (*vett)[i];
        occhio = i-1;

        while ( occhio >= 0 && (*vett)[occhio] > mano) {
            scambi++;
            (*vett)[occhio+1] = (*vett)[occhio];
            --occhio;
        }

        (*vett)[occhio+1] = mano;
    }

    return scambi;
}

int main() {
    int N = 0, temp = 0;;

    cin >> N;

    vector<int> v;

    for (int i = 0; i < N; i++) {
        cin >> temp;
        v.push_back(temp);
    }

    int s = InsertionSort(&v);
    cout << s << '\n';
    
    return 0;
}
