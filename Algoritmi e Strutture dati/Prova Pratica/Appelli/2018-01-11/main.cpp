/* 
 * Copyright 2018 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

// Se il compilatore usa uno standard precedente a C++11
// definisco `nullptr`
#if __cplusplus <= 199711L
    const int nullptr = 0;
#endif


const int p = 999149;
const int a = 1000;
const int b = 2000;

inline int Hash(const int &x, const int &S) {
    return (((a * x) + b) % p) % S;
}

struct Node {
    int x;
    Node *left;
    Node *right;

    Node(int x_):
        x(x_),
        left(nullptr),
        right(nullptr) {}
} **hashTable;

void Insert(int x, int S) {
    Node *NewNode = new Node(x);

    int hash = Hash(x, S);

    Node *actual, *prec;
    actual = prec = hashTable[hash];
    while (actual) {
        prec = actual;

        if (x <= actual->x)
            actual = actual->left;
        else
            actual = actual->right;
    }

    if (hashTable[hash] == nullptr)
        hashTable[hash] = NewNode;
    else if (x <= prec->x)
        prec->left = NewNode;
    else
        prec->right = NewNode;
}

/**
 * Ritorna ndx e nsx dell'albero root.
 * nsx e ndx DEVONO essere nulli inizialmente!
 */
void get_ndx_nsx(Node *root, int &ndx, int &nsx) {
    if (root == nullptr) return;

    if (root->left != nullptr && root->left->left == nullptr &&
        root->left->right == nullptr
    ) {
        nsx++;
    }
    if (root->right != nullptr && root->right->left == nullptr &&
        root->right->right == nullptr
    ) {
        ndx++;
    }
    get_ndx_nsx(root->left, ndx, nsx);
    get_ndx_nsx(root->right, ndx, nsx);
}

void Print(const int &S) {
    int maxidsx = 0;
    int maxiddx = 0;
    int maxndx = 0;
    int maxnsx = 0;

    for (int i = 0; i < S; ++i) {
        int nsx = 0, ndx = 0;
        get_ndx_nsx(hashTable[i], ndx, nsx);
        
        if (maxndx <= ndx) {
            maxndx = ndx;
            maxiddx = i;
        }
        if (maxnsx <= nsx) {
            maxnsx = nsx;
            maxidsx = i;
        }
    }

    cout << maxiddx << endl;
    cout << maxidsx << endl;
}

int main(void) {
    int N, S;
    cin >> N >> S;

    hashTable = new Node *[S];
    memset(hashTable, 0, sizeof(Node *) * S);

    int x;
    for (int i = 0; i < N; ++i) {
        cin >> x;
        Insert(x, S);
    }

    Print(S);

    return 0;
}
