#!/bin/python

# Copyright 2018 Giuseppe Fabiano

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#       http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import subprocess
from os import environ

s_root, b_root, f_in, f_out = sys.argv[1:]
f_in  = '%s/TestSet/%s' % (s_root, f_in)
f_out = '%s/TestSet/%s' % (s_root, f_out)

cat = subprocess.Popen([
        'cat',
        f_in
    ],
    stdout = subprocess.PIPE
)
p2 =  subprocess.Popen([
        '%s/run' % b_root
    ],
    stdin=cat.stdout,stdout=subprocess.PIPE
)
p3 =  subprocess.Popen([
        'diff', f_out, '-'
    ],
    stdin=p2.stdout
)

p3.wait()
sys.exit(p3.returncode)

