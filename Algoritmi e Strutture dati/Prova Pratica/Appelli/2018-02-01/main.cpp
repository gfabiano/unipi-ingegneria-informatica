// Copyright 2018 Giuseppe Fabiano

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>

using namespace std;

#if __cplusplus <= 199711L
    const int nullptr = 0;
#endif

struct Node {
    int x;
    bool concorde;
    int nC;
    int nD;
    Node *left;
    Node *right;

    Node(int x_):
        x(x_),
        concorde(true),
        nC(0),
        nD(0),
        left(nullptr),
        right(nullptr) {}
} *root;

void insert(int x) {
    Node *newNode = new Node(x);

    Node *actual, *prec;
    actual = prec = root;

    while(actual != nullptr) {
        prec = actual;
        if (x <= actual->x)
            actual = actual->left;
        else
            actual = actual->right;
    }

    if (prec == nullptr) {
        root = newNode;
        return;
    } else if (x <= prec->x) {
        prec->left = newNode;
    } else {
        prec->right = newNode;
    }
    newNode->concorde = !((x % 2 == 0) ^ (prec->x % 2 == 0));
}

void proprieta(Node *root, int &nC, int &nD) {
    if (root == nullptr) return;
    proprieta(root->left, root->nC, root->nD);
    proprieta(root->right, root->nC, root->nD);
    if (root->left == nullptr && root->right == nullptr) {
        nC += (root->concorde)?1:0;
        nD += (root->concorde)?0:1;
        return;
    }
    nC += root->nC;
    nD += root->nD;
}

void print(Node *root) {
    if (root == nullptr) return;
    print(root->left);
    if (root->nC - root->nD >= 0)
        cout << root->x << endl;
    print(root->right);
}

int main() {
    int N, x;
    cin >> N;

    for (int i = 0; i < N; ++i) {
        cin >> x;
        insert(x);
    }
    
    int nC, nD;
    nC = nD = 0;
    proprieta(root, nC, nD);    // O(n)
    print(root);                // O(n)

    return 0;
}

