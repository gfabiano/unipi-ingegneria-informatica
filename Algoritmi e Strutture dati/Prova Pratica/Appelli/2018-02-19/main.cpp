// Copyright 2018 Giuseppe Fabiano

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

#if __cplusplus <= 199711L
    const int nullptr = 0;
#endif

struct Node {
    int x;
    int D;
    Node *left;
    Node *right;

    Node(int x_):
        x(x_),
        D(-1),
        left(nullptr),
        right(nullptr) {}
} *root;

vector<Node*> vect;
bool vect_cmp(Node *a, Node *b) {
    return a->D > b->D || (a->D == b->D && a->x < b->x);
}

void insert(int x) {
    Node *newNode = new Node(x);

    Node *actual, *prec;
    actual = prec = root;

    while(actual != nullptr) {
        prec = actual;
        if (newNode->D >= 0 || (newNode->D == -1 && actual->x % 2 == 0)) {
            newNode->D++;
        }

        if (x <= actual->x)
            actual = actual->left;
        else
            actual = actual->right;
    }

    if (prec == nullptr)
        root = newNode;
    else if (x <= prec->x)
        prec->left = newNode;
    else
        prec->right = newNode;

    if (newNode->D >= 0)
        vect.push_back(newNode);
}

int main() {
    unsigned int N, K, x;
    cin >> N >> K;

    for (unsigned int i = 0; i < N; ++i) {
        cin >> x;
        insert(x);
    }

    K = (K > vect.size())? vect.size(): K;
    
    sort(vect.begin(), vect.end(), vect_cmp);  // O(n*log(n))
    for (unsigned int i = 0; i < K; ++i) {
        cout << vect[i]->x << endl;
    }

    return 0;
}

