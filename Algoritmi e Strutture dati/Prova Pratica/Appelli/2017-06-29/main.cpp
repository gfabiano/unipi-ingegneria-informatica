/*
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cstring>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;
using std::sort;

// Se il compilatore usa uno standard precedente a C++11
// definisco `nullptr`
#if __cplusplus <= 199711L
    const int nullptr = 0;
#endif


const int p = 999149;
const int a = 1000;
const int b = 2000;

int *DArray, *max_duplicates;
struct Node {
    int x;
    int duplicates;
    Node *left;
    Node *right;

    explicit Node(const int &n): x(n), duplicates(0) {
        left = right = nullptr;
    }
} **hashTable;

int N, K, S;

inline int Hash(const int &x) {
    return (((a * x) + b) % p) % S;
}

void Insert(const int& val) {
    int hash = Hash(val);
    Node *&root = hashTable[hash];

    Node *actual, *prec;
    actual = prec = root;
    while (actual) {
        prec = actual;

        if (val == actual->x) {
            actual->duplicates++;
            goto update_DArray;
        } else if (val <= actual->x) {
            actual = actual->left;
        } else {
            actual = actual->right;
        }
    }

    // Per non sprecare stack riutilizzo actual per il nuovo nodo, inoltre ciò
    // mi permette di riutilizzare il codice dell'etichetta update_DArray
    actual = new Node(val);
    if (root == nullptr)
        root = actual;
    else if (val <= prec->x)
        prec->left = actual;
    else
        prec->right = actual;

update_DArray:
    if (actual->duplicates > max_duplicates[hash] ||
        actual->duplicates == max_duplicates[hash] &&
        DArray[hash] < actual->x) {
        max_duplicates[hash] = actual->duplicates;
        DArray[hash] = actual->x;
    }
}

int main() {
    cin >> N >> K >> S;

    max_duplicates = new int[S];
    DArray         = new int[S];
    hashTable      = new Node *[S];

    memset(DArray, -1, sizeof(int) * S);
    memset(max_duplicates, -1, sizeof(int) * S);
    memset(hashTable, 0, sizeof(Node *) * S);

    // La complessità nel caso peggiore
    // di N inserimenti è ~O(N^3)
    for (int i = 0, tmp; i < N; ++i) {
        cin >> tmp;
        Insert(tmp);
    }

    // sort: O(S*log(S)) <<< O(N)
    sort(DArray, DArray + S);

    for (S--; S >= 0 && K > 0; --S) {
        if (DArray[S] < 0) break;

        cout << DArray[S] << endl;
        K--;
    }

    return 0;
}
