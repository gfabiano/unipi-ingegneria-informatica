## Algoritmi e strutture dati - 11 gennaio 2016

#### Esercizio 1

Applicare l'algoritmo quicksort all'array: `[50 20 60 10 8 65 12]`

Chiamata                                |perno 
:--------------------------------------:|:---:
quicksort([ 50 20 60 10 8 65 12 ], 0, 6)|   
                                        |   
                                        |   
                                        |   
                                        |   
                                        |   
                                        |    

#### Esercizio 2

Applicare l'algoritmo di Dijkstra al grafo in figura con partenza dal nodo C, indicando tutti i passaggi intermedi ed elencando i cammini minimi.

digraph finite_state_machine {
    rankdir=LR;
    size="8,5"
    node [shape = doublecircle];
    node [shape = circle];

    B -> E [ label = "1" ];
    C -> A [ label = "30" ];
    C -> B[ label = "9" ];
    C -> D [ label = "8" ];
    C -> E [ label = "14" ];
    D -> A [ label = "20" ];
    D -> E [ label = "3" ];
    E -> A [ label = "14" ];
}