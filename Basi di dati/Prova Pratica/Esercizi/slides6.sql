/*
2. Scrivere un trigger che impedisca l’inserimento di due terapie consecutive per lo stesso
paziente, caratterizzate dallo stesso farmaco, la più recente delle quali con una posologia
superiore al doppio rispetto alla precedente.
*/
drop trigger if exists blocca_terapia;

create table if not exists tmp_blocca_terapia (
    Paziente char(50),
    DataInizioTerapia date,
    Farmaco char(50),
    Posologia int,
    primary key (Paziente, DataInizioTerapia, Farmaco)
)  default charset = latin1;

delimiter $$
create trigger blocca_terapia
before insert on Terapia
for each row
begin
    declare blocca bool default false;
    declare error_msg char(150);
    
    set error_msg = concat('Non si possono inserire terapie consecutive ',
                        'con stesso farmaco e posologia superiore al doppio');
    
    SET SQL_SAFE_UPDATES = 0;
    delete from tmp_blocca_terapia;
    SET SQL_SAFE_UPDATES = 1;

    insert into tmp_blocca_terapia
        select T.Paziente, T.DataInizioTerapia, T.Farmaco, T.Posologia
        from Terapia T
        where T.Paziente = new.Paziente and
              T.DataInizioTerapia < new.DataInizioTerapia;
              
    set blocca = (
        select if(T.Paziente is null, false, true)
        from tmp_blocca_terapia T
        where T.DataInizioTerapia >= all(
            select DataInizioTerapia
            from tmp_blocca_terapia
        ) and
        T.Farmaco = new.Farmaco and
        T.Posologia * 2 < new.Posologia
    );
    
    if blocca = true then
        signal sqlstate '45000'
        set message_text = error_msg;
    end if;
end$$
delimiter ;

