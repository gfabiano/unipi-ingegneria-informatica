/*
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la realtà medica descritta dalla base di dati relazionale
-- definita dal seguente schema:

-- PAZIENTE(__CodFiscale__, Cognome, Nome, Sesso, DataNascita, Citta, Reddito)
-- MEDICO(__Matricola__, Cognome, Nome, Specializzazione, Parcella, Citta)
-- FARMACO(__NomeCommerciale__, PrincipioAttivo, Costo)
-- PATOLOGIA(__Nome__, ParteCorpo, SettoreMedico, Invalidita, PercEsenzione)
-- VISITA(__Medico, Paziente, Data__, Mutuata)
-- ESORDIO(__Paziente, Patologia, DataEsordio__, DataGuarigione, Gravita,
--         Cronica)
-- TERAPIA(__Paziente, Patologia, DataEsordio, Farmaco, DataInizioTerapia__,
--          DataFineTerapia, Posologia)

-- tip: nel file ../database_esame.pdf puoi trovare una spiegazione dello schema

/*
 * Esercizio 1 (6 punti)
 * Indicare il nome dei farmaci mai assunti prima dei venti anni d'età.
 */
select NomeCommerciale
from Farmaco F
where F.NomeCommerciale not in (
    select Farmaco
    from Paziente inner join Terapia on (CodFiscale = Paziente)
    where DataNascita + interval 20 year > DataInizioTerapia
);

/*
 * Esercizio 2 (6 punti)
 * Indicare nome e cognome dei pazienti che hanno curato sempre la stessa
 * patologia con lo stesso farmaco, per tutte le patologie contratte.
 */
select Nome, Cognome
from Paziente
where CodFiscale not in (
    select distinct T.Paziente
    from Terapia T
    group by T.Paziente, T.Patologia
    having count(distinct T.Farmaco) > 1
);

/*
 * Esercizio 3 (6 punti)
 * Indicare nome e cognome dei medici che, con gli incassi delle visite del
 * biennio 2009-2010, hanno superato il reddito mensile medio dei pazienti
 * visitati nello stesso periodo da tutti i medici.
 */
select M.Nome, M.Cognome
from Visita V inner join Medico M on V.Medico = M.Matricola
where year(`Data`) between 2009 and 2010
group by M.Matricola
having sum(M.Parcella) > (
    select avg(Reddito)
    from Paziente
    where CodFiscale in (
        select Paziente
        from Visita
        where year(`Data`) between 2009 and 2010
    )
);

/*
 * Esercizio 4 (6 punti)
 * Fra tutte le patologie non croniche rare, cioè contratte da meno del 6% dei
 * pazienti, indicare quella che si è rivelata mediamente più resistente alle
 * terapie, relativamente a tutte le volte che è stata contratta.
 */
create or replace view PatologieNonCronicheRare as
select Patologia
from Esordio
where Cronica = 'No'
group by Patologia
having count(distinct Paziente) < 0.06 * (
    select count(*)
    from Paziente
);

create or replace view MediaTerapieEffettuatePatologia as
select Patologia, avg(NumeroTerapie) as Media
from (
    select Patologia, count(*) as NumeroTerapie
    from Terapia T natural join PatologieNonCronicheRare
    group by T.Paziente, T.DataEsordio, T.Patologia
) A
group by Patologia;

select Patologia
from MediaTerapieEffettuatePatologia
where Media = (
    select max(Media)
    from MediaTerapieEffettuatePatologia
);

/*
 * Esercizio 5 (9 punti)
 * Un effetto collaterale è una patologia insorta mentre se ne stava curando
 * un'altra con un dato farmaco. La frequenza di un effetto collaterale di un
 * farmaco è quantificabile come la percentuale dei pazienti che ne sono stati
 * vittima, rispetto a tutti i pazienti curati con lo stesso farmaco. Qualora un
 * paziente stia effettuando più terapie, il manifestarsi di un effetto
 * collaterale è imputabile al solo farmaco oggetto della terapia iniziata da
 * meno tempo rispetto al verificarsi dell'effetto collaterale. Creare e
 * popolare una tabella EFFETTOCOLLATERALE contenente, per ogni farmaco, tutti i
 * suoi effetti collaterali, ciascuno abbinato alla relativa frequenza. Scrivere
 * infine un trigger (eventualmente anche più di uno) per mantenere aggiornata
 * la tabella.
 */
delimiter !
drop table if exists EffettoCollaterale!
create table EffettoCollaterale (
    Farmaco     char(50),
    Patologia   char(50),
    Frequenza   double,
    primary key(Farmaco, Patologia)
) Engine=InnoDB default charset=latin1!

drop procedure if exists fullRefresh_EffettoCollaterale!
create procedure fullRefresh_EffettoCollaterale()
begin
    insert into EffettoCollaterale
    select T.Farmaco, E.Patologia,
        -- 100:PazientiTotFarmaco=Freq:EffettiCollaterali
        -- Freq = 100 * EffettiCollaterali / PazientiTotFarmaco
        (
            100 * count(distinct E.Paziente) /
            (select count(distinct Paziente)
             from Terapia
             where Farmaco = T.Farmaco
            )
        ) as Freq
    from Esordio E inner join Terapia T on (
        E.Paziente = T.Paziente and
        E.Patologia <> T.Patologia and
        E.DataEsordio > T.DataInizioTerapia and (
            T.DataFineTerapia is null or
            E.DataEsordio < T.DataFineTerapia
        )
    )
    where not exists (
        select *
        from Terapia T1
        where T1.Paziente = E.Paziente and
            T1.Patologia <> E.Patologia and
            T1.DataInizioTerapia > T.DataInizioTerapia and 
            T1.DataInizioTerapia < E.DataEsordio and (
                T1.DataFineTerapia is null or
                E.DataEsordio < T1.DataFineTerapia
            )
    )
    group by T.Farmaco, E.Patologia
    ;
end!

call fullRefresh_EffettoCollaterale()!

drop trigger if exists EffettoCollateraleNuovoEsordio!
create trigger EffettoCollateraleNuovoEsordio
after insert on Esordio
for each row
begin
    declare farmaco_        char(50);
    declare pazTotFarmaco_  int;

    -- Se il nuovo esordio costituisce un nuovo effetto collaterale e il
    -- paziente non aveva presentato lo stesso in un altro momento allora su
    -- `farmaco_` troverò il farmaco che lo ha causato.
    set farmaco_ = (
        select T.Farmaco
        from Terapia T
        where T.Paziente = new.Paziente and
        not exists (
            select *
            from Terapia T1
            where T1.DataInizioTerapia > T.DataInizioTerapia and
                new.DataEsordio > T1.DataInizioTerapia and
                (T1.DataFineTerapia is null or
                 T1.DataFineTerapia > new.DataEsordio
                )
        ) and not exists (
            select *
            from Esordio E inner join Terapia T1 on (
                E.Paziente = new.Paziente and
                E.Paziente = T1.Paziente and
                E.Patologia = new.Patologia and
                E.Patologia <> T1.Patologia and
                E.DataEsordio <> new.DataEsordio and
                E.DataEsordio > T1.DataInizioTerapia and (
                    T1.DataFineTerapia is null or
                    T1.DataFineTerapia > E.DataEsordio
                )
            )
            where not exists (
                select *
                from Terapia T2
                where T2.DataInizioTerapia > T1.DataInizioTerapia and
                    E.DataEsordio > T2.DataInizioTerapia and
                    (T2.DataFineTerapia is null or
                     T2.DataFineTerapia > E.DataEsordio
                    )
            ) 
        )
    );

    if farmaco_ is not null then
    begin
        set pazTotFarmaco_ = (
            select count(distinct Paziente)
            from Terapia
            where Farmaco = farmaco_
        );

        -- Se l'effetto collaterale era già stato registrato per altri pazienti
        -- aggiorno la frequenza, altrimenti inserisco il nuovo effetto
        -- collaterale nella tabella
        if exists (
            select * from EffettoCollaterale
            where Farmaco = farmaco_ and Patologia = new.Patologia
        ) then
            update EffettoCollaterale
            set Frequenza = (
                100 * (1 +
                    (Frequenza * (pazTotFarmaco_ - 1)/ 100)
                ) / pazTotFarmaco_
            )
            where (Farmaco, Patologia) = (farmaco_, new.Patologia);
        else
            insert into EffettoCollaterale(Farmaco, Patologia, Frequenza)
                values(farmaco_, new.Patologia, 100/pazTotFarmaco_);
        end if;
    end;
    end if;
end!
delimiter ;
