/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Esercizio 1 (10 punti)
 * Considerate le patologie gastroenteriche, scrivere una query che restituisca
 * il nome commerciale dei farmaci utilizzati da almeno un paziente in almeno
 * due terapie relative alla stessa patologia, e il numero di tali pazienti per
 * ciascuno di tali farmaci.
 */
select A.Farmaco,
    count(DISTINCT A.Paziente) AS NPazienti
from (
    select *
    from Patologia P
    inner join Terapia T
        on (P.Nome = T.Patologia)
    where P.settoreMedico = 'Gastroenterologia'
    group by T.Paziente,
        T.Patologia,
        T.Farmaco
    having count(*) >= 2
    ) AS A
group by A.Farmaco;

/*
 * Esercizio 2 (10 punti)
 * Implementare una business rule che consenta l'inserimento di una visita
 * mutuata relativa a un settore medico solamente se il paziente non è
 * attualmente in terapia con un farmaco indicato per patologie dello stesso
 * settore medico e le sue visite mutuate effettuate con medici specialisti di
 * quel settore medico, dall'inizio dell'anno, non superino del 20% le visite
 * non mutuate.
 */
delimiter !
drop trigger if exists trig_mutuata!
create trigger trig_mutuata before insert on Visita
for each row
begin
    declare settore_ char(50);

    if new.Mutuata = 1 then
    begin
        select Specializzazione
        into settore_
        from Medico
        where Matricola = new.Medico;

        if exists (
            select *
            from Terapia T
            inner join Indicazione I
                on (T.Farmaco = I.Farmaco)
            inner join Patologia P
                on (P.Nome = I.Farmaco)
            where T.Paziente = new.Paziente
                and T.DataFineTerapia is null
                and P.settoreMedico = settore_
            )
        and (
            select sum(V.Mutuata) > 1.20 * sum(NOT V.Mutuata)
            from Visita V
            inner join Medico M
                on (V.Medico = M.Matricola)
            where M.Specializzazione = settore_
                and V.Paziente = new.Paziente
            ) then
            signal sqlstate '45000'
            set message_text = 'Inserimento visita non consentito';
        end if;
        end;
    end if;
end!
delimiter ;

/*
 * Esercizio 3 (13 punti)
 * Implementare una stored procedure healthy_patients_in_period() che, ricevute
 * in ingresso due date _from e _to, restituisca, come result set, il codice
 * fiscale dei pazienti che nel lasso di tempo compreso fra le due date
 * risultavano sani, ovverosia, non avevano patologie in essere. Inoltre, per
 * ogni paziente del risultato, la stored procedure deve restituire da quanto
 * tempo (in giorni) il paziente risultava sano prima di _from e per quanto
 * tempo (in giorni) lo è stato dopo _to. Si presti attenzione al fatto che in
 * generale gli esordi possono sovrapporsi temporalmente e che quindi, in un
 * dato istante, un paziente può essere affetto da più patologie. Si gestiscano
 * i contesti di errore dovuti a input non validi, interrompendo forzatamente
 * l'elaborazione.
 */
delimiter !
drop procedure if exists healthy_patients_in_period!
create procedure healthy_patients_in_period (
    in _from date,
    in _to date
    )
begin
    if _from > _to then
        signal sqlstate '45000'
        set message_text = '\nInput errato:\n _from deve essere minore di _to!';
    end if ;
    select CodFiscale,
        datediff(_from, (
                select DataGuarigione
                from Esordio
                where Paziente = CodFiscale and
                    DataGuarigione < _from
                order by DataGuarigione desc limit 1
                )
        ) as DaQuanto,
        datediff((
                select DataGuarigione
                from Esordio
                where Paziente = CodFiscale and
                    DataEsordio > _from
                order by DataEsordio asc limit 1
                ), _to) as PerQuanto
    from Paziente P
    where not exists (
            select Paziente
            from Esordio E
            where E.Paziente = P.CodFiscale
                and (
                    (E.DataEsordio between _from and _to) or
                    (E.DataGuarigione between _from and _to) or
                    (E.DataEsordio < _from and E.DataGuarigione > _to)
                )
            );
end!
delimiter ;

-- Esempio uso stored procedure
-- Se non ci sono esordi precedenti a _from o successivi a _to nei rispettivi
-- campi viene restituito NULL anziché il numero di giorni
call healthy_patients_in_period('2010-01-01', '2010-02-01');
