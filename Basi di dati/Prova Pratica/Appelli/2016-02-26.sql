/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * Esercizio 1 (10 punti)
 * Scrivere una query che restituisca le coppie mese-anno del triennio 2013-2015
 * caratterizzate da terapie tutte iniziate e concluse nello stesso mese, a
 * prescindere dall'esito.
 */
select 
    month(T1.DataInizioTerapia) as Mese,
    year(T1.DataInizioTerapia) as Anno
from
    Terapia T1 left join
    Terapia T2 on (
        T1.Paziente = T2.Paziente and
        T1.DataEsordio = T2.DataEsordio and
        T1.Patologia = T2.Patologia and
        T1.Farmaco = T2.Farmaco and
        T1.DataInizioTerapia = T2.DataInizioTerapia and
        month(T2.DataInizioTerapia) = month(T2.DataFineTerapia)
    )
where year(T1.DataEsordio) between 2013 and 2015
group by year(T1.DataInizioTerapia), month(T1.DataInizioTerapia)
having sum(distinct T2.Paziente is null) = 0;

/*
 * Esercizio 2 (11 punti)
 * Considerati come pazienti target i pazienti ambosessi affetti da una
 * patologia neurologica cronica, scrivere una query che restituisca il nome
 * commerciale dei farmaci indicati per al più due patologie neurologiche,
 * utilizzati maggiormente da pazienti target di sesso femminile, per la cura
 * dell'unica patologia cronica da cui sono affetti.
 */         
create or replace view PazientiTarget as
select E.Paziente, E.Patologia, E.DataEsordio, P.Sesso
from
    Patologia PA inner join
    Esordio E on (
        PA.SettoreMedico = 'Neurologia' and
        PA.Nome = E.Patologia and
        E.Cronica = 'Si'
    ) inner join
    Paziente P on E.Paziente = P.CodFiscale
group by E.Paziente, P.Sesso
having count(*) = 1; -- Posso proiettare tranquillamente con questa condizione

select T.Farmaco
from
    PazientiTarget PT natural join
    Terapia T
    where T.Farmaco in (
        select I.Farmaco
        from
            Indicazione I inner join
            Patologia PA on (
                I.Patologia = PA.Nome and
                PA.SettoreMedico = 'Neurologia'
            )
        group by I.Farmaco
        having count(*) <= 2
    )
group by T.Farmaco
having sum(PT.Sesso = 'F') > sum(PT.Sesso = 'M');

/*
 * Esercizio 3 (12 punti)
 * Implementare una stored procedure discount_drug() che riceva come parametri
 * una specializzazione medica e un intero k e riduca il costo dei primi farmaci
 * più utilizzati nel 2015 per la cura di patologie relative alla
 * specializzazione medica s, applicando a ciascun farmaco i uno sconto  
 * d_i∈ [0,1) pari a
 *
 *                      ⎛        k                 ⎞
 *                      ⎜       ____               ⎟
 *                      ⎜       ╲                  ⎟
 *                      ⎜        ╲       ______    ⎟
 *          T_s,i       ⎜         ╲     ╱  1       ⎟
 * d_i = ──────────── * ⎜ 1 + k * ╱    ╱  ────     ⎟
 *        Max(T_s,i)    ⎜        ╱   ╲╱   T_s,i    ⎟
 *                      ⎜       ╱                  ⎟
 *                      ⎜       ‾‾‾‾               ⎟
 *                      ⎝      i = 1               ⎠
 * dove T_s,i>0 è il numero di terapie (indipendentemente dall'esito) che hanno
 * impiegato il farmaco i per curare patologie della specializzazione s nel 2015.
 */

delimiter !
drop procedure if exists discount_drug!
create procedure discount_drug(
    in s char(50),
    in k int
)
begin
create temporary table if not exists FarmaciTarget(
    Farmaco char(50) not null primary key,
    T_si int not null default 0
)Engine=InnoDB default charset=latin1;

truncate table FarmaciTarget;

insert into FarmaciTarget
select Farmaco, T_si
from (
    select 
        if (@prec = 0,
            @rank := 1 + least(0, @prec := A.T_si),
            if(
                @prec = A.T_si,
                @rank + least(0, @prec := A.T_si),
                @rank := @rank + 1 + least(0, @prec := A.T_si)
            )
        )
        as Rank,
        A.Farmaco,
        A.T_si
    from (
        select T.Farmaco, count(*) as T_si
        from Terapia T inner join
            Patologia PA on ( 
                T.Patologia = PA.Nome and
                year(T.DataInizioTerapia) = 2015 and
                PA.SettoreMedico = s
            )
        group by T.Farmaco
    ) A, (select (@prec := 0)) as N
    order by T_si desc
) B
where B.Rank <=  k
;

begin
declare farmaco_ char(50);
declare T_si_ int default 0;
declare max_ int default 0;
declare radix double default 0;
declare end_ tinyint default 0;

declare cursor_ cursor for
select * from FarmaciTarget;

declare continue handler for not found set end_ = 1;

set radix = (
    select sum(sqrt(1/T_si))
    from FarmaciTarget
);

set max_ = (
    select max(T_si)
    from FarmaciTarget
);

open cursor_;
loop_: loop
    fetch cursor_ into farmaco_, T_si_;
    if end_ = 1 then
        leave loop_;
    end if;

    -- (T_si_/max_)*(1+k*radix) restituisce valori che la maggior parte delle 
    -- volte superano di molto 1. Ciò va contro le condizioni dell'esercizio e
    -- siccome non sono riuscito a trovare errori potrei aver male interpretato
    -- il problema o semplicemente il testo potrebbe essere errato.
    -- Se trovate l'errore o avete qualche dritta da darmi contattatemi così da
    -- poter correggere l'errore.
    update Farmaco
    set Costo = Costo - (T_si_/max_)*(1+k*radix)
    where NomeCommerciale = farmaco_;
end loop loop_;
end;
    
end!
delimiter ;

-- call discount_drug('Cardiologia', 2)
