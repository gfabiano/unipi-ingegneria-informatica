/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la base di dati relazionale definita dal seguente schema:

-- ATTORE(__CodAttore__, Cognome, Nome, Sesso, DataNascita, Nazionalita)
-- FILM(__CodFilm__, Titolo, CodRegista, Anno)
-- INTERPRETAZIONE(__CodFilm, CodAttore, Personaggio__, SessoPersonaggio)
-- REGISTA(__CodRegista__, Cognome, Nome, Sesso, DataNascita, Nazionalita)
-- PRODUZIONE(CasaProduzione, Nazionalita, __CodFilm__, Costo, IncassoSala)
-- NOLEGGIO(__CodFilm__, IncassoVideo, IncassoDVD)

-- Esprimere le seguenti richieste in linguaggio SQL. Ogni esercizio, risolto 
-- correttamente, assegna 6 punti.

/*
 * Esercizio 1
 * Senza utilizzare viste né join, indicare nome e cognome dei registi che
 * hanno diretto film il cui incasso nelle sale è stato inferiore alla media
 * degli incassi dei film statunitensi, e non è stato nemmeno sufficiente a
 * ripagare il costo di produzione.
 */
select R.Nome, R.Cognome
from Regista R
where R.CodRegista in (
    select F.CodRegista
    from Film F 
    where F.CodFilm in (
        select P.CodFilm
        from Produzione P
        where
            P.Costo > P.IncassoSala and
            P.IncassoSala < (
                select avg(IncassoSala)
                from Produzione
                where Nazionalita = 'Stati Uniti'
            )
    )
);

/*
 * Esercizio 2
 * Selezionare nome e cognome dei registi i cui film hanno incassato tutti
 * almeno 1000 euro.
 */
select R.Nome, R.Cognome
from Regista R
where 1000 <= all (
    select P.IncassoSala
    from Film F natural join Produzione P 
    where F.CodRegista = R.CodRegista
);

/*
 * Esercizio 3
 * Indicare, per ogni attore, il suo nome e cognome, e il numero di film nei
 * quali interpreta un personaggio di sesso diverso dal suo.
 */
select A.Nome, A.Cognome, sum(A.Sesso <> I.SessoPersonaggio)
from Attore A natural join Interpretazione I
group by A.CodAttore;

/*
 * Esercizio 4
 * Indicare, per ogni casa di produzione, il nome della casa di produzione e il
 * titolo del film prodotto da essa avente il più alto costo di produzione e il
 * più basso incasso in sala. Suggerimento: la richiesta è per ogni casa di
 * produzione, quindi tutte le case di produzione dovranno comparire nel
 * risultato. Riflettere sull'importanza del valore NULL…
 */

-- Per ogni casa di produzione c'è almeno un film. Questo implica che ogni casa
-- di produzione verrà proiettata.
-- Il valore `NULL…` quindi non è così importante come ritiene il suggerimento.
select P.CasaProduzione, F.Titolo
from Produzione P natural join Film F
where (P.Costo, P.IncassoSala) = (
    select max(Costo), min(IncassoSala)
    from Produzione
    where CasaProduzione = P.CasaProduzione
)
group by P.CasaProduzione;

/*
 * Esercizio 5
 * Aumentare del 10% l'incasso in sala dei film che hanno ottenuto un incasso
 * complessivo, fra noleggio video e DVD, superiore ai ⅔ dell'incasso
 * ottenuto in sala. Terminare la query con il carattere ';' e inserire di
 * seguito SELECT * FROM Produzione, prima di copiarla nel file soluzione.txt.
 */
update Produzione
set IncassoSala = 1.10 * IncassoSala
where CodFilm in (
    select CodFilm
    from Noleggio
    where IncassoVideo + IncassoDVD > (IncassoSala * 2) / 3
);
