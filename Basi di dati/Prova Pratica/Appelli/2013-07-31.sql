/*
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la realtà medica descritta dalla base di dati relazionale
-- definita dal seguente schema:

-- PAZIENTE(__CodFiscale__, Cognome, Nome, Sesso, DataNascita, Citta, Reddito)
-- MEDICO(__Matricola__, Cognome, Nome, Specializzazione, Parcella, Citta)
-- FARMACO(__NomeCommerciale__, PrincipioAttivo, Costo)
-- PATOLOGIA(__Nome__, ParteCorpo, SettoreMedico, Invalidita, PercEsenzione)
-- VISITA(__Medico, Paziente, Data__, Mutuata)
-- ESORDIO(__Paziente, Patologia, DataEsordio__, DataGuarigione, Gravita,
--         Cronica)
-- TERAPIA(__Paziente, Patologia, DataEsordio, Farmaco, DataInizioTerapia__,
--          DataFineTerapia, Posologia)

-- tip: nel file ../database_esame.pdf puoi trovare una spiegazione dello schema

/*
 * Esercizio 1 (6 punti)
 * Scrivere un trigger che permetta l'inserimento di terapie per la cura di
 * patologie invalidanti oltre il 60% solo se, al massimo una settimana prima
 * dell'inizio della terapia, il relativo paziente è stato visitato da un
 * medico con specializzazione coerente alla patologia oggetto di terapia.
 */
delimiter !
drop trigger if exists TerapiaInvalidante!
create trigger TerapiaInvalidante
before insert on Terapia
for each row
begin
    declare spec_   char(50);

    select SettoreMedico
    into spec_
    from Patologia
    where Nome = new.Patologia and Invalidita > 60;

    if spec_ is not null and not exists (
        select V.Paziente
        from Visita V inner join Medico M on (
            V.Paziente = new.Paziente and
            V.Medico = M.Matricola
        ) where M.Specializzazione = spec_ and
            V.Data between new.DataInizioTerapia - interval 7 day and
                new.DataInizioTerapia
    ) then
        signal sqlstate '45000'
        set message_text = 'La terapia non puo` essere inserita!';
    end if;
end!
delimiter ;

/*
 * Esercizio 2 (6 punti)
 * Per ciascuna specializzazione, indicarne il nome, e il nome e cognome del
 * medico che ha visitato più pazienti mai visitati in precedenza da altri
 * medici della stessa specializzazione.
 */
create or replace view MedicoNuoviPazienti as
select
    M.Matricola, M.Nome, M.Cognome,
    M.Specializzazione,
    count(distinct Paziente) as NuoviPazienti
from Medico M inner join Visita V on M.Matricola = V.Medico
where not exists (
    select *
    from Medico inner join Visita on Matricola = Medico
    where 
        V.Paziente = Paziente and
        M.Matricola <> Matricola and
        M.Specializzazione = Specializzazione and
        V.`Data` > `Data`
)
group by V.Medico;

select MNP.Specializzazione, MNP.Nome, MNP.Cognome
from MedicoNuoviPazienti MNP
where NuoviPazienti = (
    select max(NuoviPazienti)
    from MedicoNuoviPazienti
    where Specializzazione = MNP.Specializzazione
);

/*
 * Esercizio 3 (6 punti)
 * Relativamente ad ogni farmaco indicato per la cura dell'insonnia, indicarne
 * il nome commerciale e la differenza percentuale fra la posologia media di
 * tutte le terapie effettuate con ciascuno di essi, rispetto alla rispettiva
 * dose giornaliera consigliata nelle indicazioni.
 */
select NomeCommerciale,
    -- Differenza percentuale = (ValFinale-ValIniziale)/ValIniziale*100
    ((
        select avg(Posologia)
        from Terapia
        where Farmaco = NomeCommerciale
    ) - DoseGiornaliera)/DoseGiornaliera * 100 as Differenza
from Farmaco F inner join Indicazione I on F.NomeCommerciale = I.Farmaco
where I.Patologia = 'Insonnia';

/*
 * Esercizio 4 (6 punti)
 * Considerata ciascuna patologia a carico dello stomaco, indicarne il nome, e
 * il principio attivo contenuto in più farmaci indicati per la cura della
 * stessa patologia.
 */
select PA.Nome, F.PrincipioAttivo
from Patologia PA inner join
Indicazione I on I.Patologia = PA.Nome inner join
Farmaco F on F.NomeCommerciale = I.Farmaco 
where PA.ParteCorpo = 'Stomaco'
group by PA.Nome, F.PrincipioAttivo
having count(*) >= all (
    select count(*)
    from Indicazione inner join Farmaco on NomeCommerciale = Farmaco
    where Patologia = PA.Nome
    group by PrincipioAttivo
);

/*
 * Esercizio 5 (10 punti)
 * Indicare il numero di pazienti di Pisa che, nel triennio 2008-2010, per
 * curare ogni patologia contratta, hanno sempre assunto solamente il farmaco
 * più economico fra tutti quelli indicati per la cura della stessa patologia,
 * a meno di eccezioni verificatesi in non più di un quinto delle terapie,
 * benché, per ciascuna terapia non eccezionale, il risparmio ottenuto sul
 * costo di essa, rispetto ad aver fatto la terapia col farmaco più costoso
 * adatto alla cura della stessa patologia, si sia rivelato irrisorio, cioè
 * inferiore al 2% del loro reddito.
 */

-- Come costo considero il costo del singolo pezzo nella confezione. In questo
-- modo alcuni farmaci si rivelano in realtà meno costosi.
-- La differenza del resultset fra questa e la soluzione del professore è
-- causata da questa disparità
create or replace view MaxMinFarmacoPatologia as
select
    I.Patologia,
    I.Farmaco,
    F.Costo/F.Pezzi as CPezzoMin,
    F1.Costo/F1.Pezzi as CPezzoMax
from
    Indicazione I inner join
    Farmaco F on I.Farmaco = F.NomeCommerciale inner join
    Indicazione I1 on I.Patologia = I1.Patologia inner join
    Farmaco F1 on (
        I1.Farmaco = F1.NomeCommerciale and
        F.Costo/F.Pezzi <= F1.Costo/F1.Pezzi
    )
where (F.Costo/F.Pezzi, F1.Costo/F1.Pezzi) = (
    select min(Costo/Pezzi), max(Costo/Pezzi)
    from Indicazione inner join Farmaco
    where Farmaco = NomeCommerciale and
        Patologia = I.Patologia
);

create or replace view TerapieTarget as
select *
from Terapia
where year(DataInizioTerapia) between 2008 and 2010
group by Paziente, Patologia, DataEsordio
having count(*) = 1
;

create or replace view TerapieNonEccezionali as
select T.Paziente, 
    T.Patologia,
    -- Se la terapia non è ancora finita calcolo il costo fino all'ultimo
    -- giorno del 2010
        MMFP.CPezzoMin*Posologia*
        datediff(
            ifnull(T.DataFineTerapia,'2010-12-31'),
            T.DataInizioTerapia
        ) as CostoMinimo,
        MMFP.CPezzoMax*Posologia*
        datediff(
            ifnull(T.DataFineTerapia,'2010-12-31'),
            T.DataInizioTerapia
        ) as CostoMassimo
from
    TerapieTarget T natural join
    MaxMinFarmacoPatologia MMFP
;

select count(*) as NumeroPazienti
from TerapieNonEccezionali TNE inner join
    Paziente P on P.CodFiscale = TNE.Paziente
where P.Citta = 'Pisa' and TNE.Paziente in (
    select Paziente
    from TerapieNonEccezionali TNE1
    group by Paziente
    having count(*) >= (4/5) * (
        select count(*)
        from TerapieTarget 
        where Paziente = TNE1.Paziente
    )
)
group by TNE.Paziente
-- Controllo che CIASCUNA terapia non eccezionale soddisfi la condizione che
-- il risparmio ottenuto rispetto la terapia più costosa è stato irrisorio
having sum(TNE.CostoMinimo - TNE.CostoMinimo < 0.02 * P.Reddito) = count(*);
