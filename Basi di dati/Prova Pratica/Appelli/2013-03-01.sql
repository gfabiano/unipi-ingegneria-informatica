/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la realtà medica descritta dalla base di dati relazionale
-- definita dal seguente schema:

-- PAZIENTE(__CodFiscale__, Cognome, Nome, Sesso, DataNascita, Citta, Reddito)
-- MEDICO(__Matricola__, Cognome, Nome, Specializzazione, Parcella, Citta)
-- FARMACO(__NomeCommerciale__, PrincipioAttivo, Costo)
-- PATOLOGIA(__Nome__, ParteCorpo, SettoreMedico, Invalidita, PercEsenzione)
-- VISITA(__Medico, Paziente, Data__, Mutuata)
-- ESORDIO(__Paziente, Patologia, DataEsordio__, DataGuarigione, Gravita,
--         Cronica)
-- TERAPIA(__Paziente, Patologia, DataEsordio, Farmaco, DataInizioTerapia__,
--          DataFineTerapia, Posologia)

-- tip: nel file ../database_esame.pdf puoi trovare una spiegazione dello schema

-- Esprimere le seguenti richieste in linguaggio SQL. Ogni esercizio risolto
-- correttamente assegna 6 punti.

/*
 * Esercizio 1
 * Indicare nome e cognome dei pazienti che sono stati visitati da tutti i
 * i medici.
 */
select P.Nome, P.Cognome
from
    Paziente P inner join
    Visita V on(
        P.CodFiscale = V.Paziente
    )
group by P.CodFiscale
having count(distinct V.Medico) = (
    select count(*)
    from Medico
);

/*
 * Esercizio 2
 * Indicare la variazione percentuale dell vistite ortopediche fra il 2010 e il
 * 2012
 */
select 100 * (VisiteFinali - VisiteIniziali)/VisiteIniziali as VarPercentuale
from (
    select
        sum(year(V.`Data`) = 2010) as VisiteIniziali,
        sum(year(V.`Data`) = 2012) as VisiteFinali
    from
        Visita V inner join
        Medico M on V.Medico = M.Matricola
    where
        M.Specializzazione = 'Ortopedia' and 
        year(V.`Data`) in (2010, 2012)
) as A;

-- La seguente procedura rende l'esercizio 2 generico.
-- Presa una specializzazione `s`, un anno iniziale `ai`, e una anno finale `af`
-- calcola la variazione percentuale delle visite della specializzazione `s`
-- fra `ai` e `af`.
delimiter !
drop procedure if exists varperc_visite!
create procedure varperc_visite(
    in s    char(50),
    in ai   int,
    in af   int
)
begin
    select 100 * (VisiteFinali - VisiteIniziali)/VisiteIniziali as VarPercentuale
    from (
        select
            sum(year(V.`Data`) = ai) as VisiteIniziali,
            sum(year(V.`Data`) = af) as VisiteFinali
        from
            Visita V inner join
            Medico M on V.Medico = M.Matricola
        where
            M.Specializzazione = s and 
            year(V.`Data`) in (ai, af)
    ) as A;
end!
delimiter ;

call varperc_visite('Ortopedia', 2010, 2012);

/*
 * Esercizio 3
 * Indicare, per ogni patologia contratta in forma non cronica, il nome della
 * patologia e il principio attivo del farmaco che ha portato il maggior numero
 * di pazienti alla guarigione.
 */
create or replace view EsordiGuariti as
select
    T.Paziente,
    T.Patologia,
    T.DataEsordio,
    T.Farmaco,
    T.DataFineTerapia,
    E.DataGuarigione
from
    Terapia T inner join
    Esordio E using(Paziente, Patologia, DataEsordio)
where E.DataGuarigione is not null and E.Cronica = 'No'
;

-- Considero il farmaco che ha portato alla guarigione l'ultimo utilizzato
create or replace view Guarigioni as
select EG.Patologia, EG.Farmaco, count(distinct EG.Paziente) as Guariti
from EsordiGuariti EG
where (DataFineTerapia) = (
    select max(DataFineTerapia)
    from EsordiGuariti
    where (Paziente, Patologia, DataEsordio) = (
        EG.Paziente, EG.Patologia, EG.DataEsordio
    )
)
group by Patologia, Farmaco;

select G.Patologia, F.PrincipioAttivo
from Guarigioni G inner join Farmaco F on G.Farmaco = F.NomeCommerciale
where G.Guariti = (
    select max(Guariti)
    from Guarigioni
    where Patologia = G.Patologia
);

/*
 * Esercizio 4
 * Indicare il dosaggio giornaliero medio del farmaco Aulin assunto da pazienti
 * di età superiore ai 65 anni che non sono mai stati visitati da un nefrologo.
 */
select avg(T.Posologia)
from
    Paziente P inner join
    Terapia T on (
        P.DataNascita + interval 65 year < current_date and
        P.CodFiscale = T.Paziente and
        T.Farmaco = 'Aulin'
    )
where T.Paziente not in (
    select distinct V.Paziente
    from
        Visita V inner join
        Medico M on V.Medico = M.Matricola
    where M.Specializzazione = 'Nefrologia'
);

/*
 * Esercizio 5
 * Indicare nome, cognome e reddito dei pazienti affetti da malattie cardiache
 * che sono stati visitati da almeno uno dei due migliori cardiologi di Pisa.
 * Assumere che un cardiologo sia tanto migliore quanto più è alta la sua
 * parcella.
 */

-- Questo esercizio l'ho risolto in modo complicato di proposito per utilizzare
-- le analytic functions. Niente di personale.
drop temporary table if exists RankMedicoPisa;
create temporary table RankMedicoPisa as (
    -- Non vengono gestiti gli ex-equo, se due medici hanno parcella uguale non
    -- avranno lo stesso rank
    select
        if (@spec = M.Specializzazione,
            @rank := @rank + 1,
            @rank := 1 + least(0, @spec := M.Specializzazione)
        ) as Rank,
        M.Matricola,
        M.Specializzazione,
        M.Parcella,
        M.Citta
    from Medico M, (select (@spec := ''), (@rank:= 1)) as S
    where M.Citta = 'Pisa'
    order by M.Specializzazione, M.Parcella desc
);

select distinct P.Nome, P.Cognome, P.Reddito
from 
    Paziente P inner join
    Esordio E on E.Paziente = P.CodFiscale inner join
    Patologia PA on (PA.Nome = E.Patologia)
where
    PA.ParteCorpo = 'Cuore' and
    E.DataGuarigione is null and
    E.Paziente in (
        select distinct V.Paziente
        from Visita V
        where V.Medico in (
                select Matricola
                from RankMedicoPisa
                where
                    Specializzazione = 'Cardiologia' and
                    rank <= 2
            )
        )
;
