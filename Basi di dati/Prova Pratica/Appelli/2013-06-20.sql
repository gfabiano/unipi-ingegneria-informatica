/*
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la realtà medica descritta dalla base di dati relazionale
-- definita dal seguente schema:

-- PAZIENTE(__CodFiscale__, Cognome, Nome, Sesso, DataNascita, Citta, Reddito)
-- MEDICO(__Matricola__, Cognome, Nome, Specializzazione, Parcella, Citta)
-- FARMACO(__NomeCommerciale__, PrincipioAttivo, Costo)
-- PATOLOGIA(__Nome__, ParteCorpo, SettoreMedico, Invalidita, PercEsenzione)
-- VISITA(__Medico, Paziente, Data__, Mutuata)
-- ESORDIO(__Paziente, Patologia, DataEsordio__, DataGuarigione, Gravita,
--         Cronica)
-- TERAPIA(__Paziente, Patologia, DataEsordio, Farmaco, DataInizioTerapia__,
--          DataFineTerapia, Posologia)

-- tip: nel file ../database_esame.pdf puoi trovare una spiegazione dello schema

/*
 * Esercizio 1 (6 punti)
 * Indicare nome e cognome dei pazienti che hanno contratto tutte le patologie.
 */
select P.Nome, P.Cognome
from Paziente P
where P.CodFiscale in (
    select Paziente
    from Esordio E
    group by Paziente
    having count(distinct E.Patologia) = (
        select count(*)
        from Patologia
    )
);

/*
 * Esercizio 2  (6 punti)
 * Indicare nome e cognome del paziente visitato più volte mentre era affetto
 * da almeno una patologia. Se più pazienti rispettano la suddetta condizione,
 * indicarli tutti.
 */
create or replace view VisiteDurantePatologia as
select distinct E.Paziente, count(*) as Visite
from Esordio E inner join Visita V
    using(Paziente)
where (E.DataGuarigione is null and V.Data > E.DataEsordio) or
    (V.Data between E.DataEsordio and E.DataGuarigione)
group by E.Paziente, E.Patologia, E.DataEsordio;

select P.Nome, P.Cognome
from Paziente P
where P.CodFiscale in (
    select Paziente
    from VisiteDurantePatologia
    where Visite = (
        select max(Visite)
        from VisiteDurantePatologia
    )
);

/*
 * Esercizio 2  (6 punti)
 * Indicare nome e cognome del paziente visitato più volte non era affetto da
 * patologie. Se più pazienti rispettano la suddetta condizione, indicarli
 * tutti.
 */
create or replace view VisiteSani as
select V.Paziente, count(*) as Visite
from Visita V
where not exists (
    select Paziente
    from Esordio
    where Paziente = V.Paziente and
        if(
            DataGuarigione is null,
            V.Data >= DataEsordio,
            V.Data between DataEsordio and DataGuarigione
        )
)
group by V.Paziente;

select P.Nome, P.Cognome
from Paziente P
where P.CodFiscale in (
    select Paziente
    from VisiteSani
    where Visite = (
        select max(Visite)
        from VisiteSani
    )
);


/*
 * Esercizio 3 (6 punti)
 * Indicare il [nome|numero|Turno 2] delle patologie contratte esclusivamente
 * [|da pazienti di pisa|A.A. Prec] dopo il compimento del sessantesimo anno di
 * età.
 */
-- [Turno 2] {
-- select count(PA.Nome)
select PA.Nome
-- }
from Patologia PA
where PA.Nome not in (
    select distinct Patologia
    from Esordio E inner join Paziente P on (E.Paziente = P.CodFiscale)
    where P.DataNascita + interval 60 year < E.DataEsordio
    -- [A.A. Prec] {
    -- and P.Citta <> 'Pisa'
    -- }
);

/*
 * Esercizio 4 (7 punti)
 * Per ciascun settore medico, indicarne il nome e il costo totale dei farmaci
 * oggetto di terapie effettuate [nel triennio 2008-2010|cinque anni fa|Turno 2]
 * per curare patologie contratte per la prima volta nello stesso periodo. Al
 * costo dei farmaci sottrarre la percentuale di esenzione, ove prevista.
 */
select
    PA.SettoreMedico,
    sum(
        -- Trovo il numero di confezioni di farmaco usate
        datediff(T.DataFineTerapia, DataInizioTerapia) * T.Posologia / F.Pezzi
        -- Moltiplico per il costo di ogni confezione, togliendo l'esenzione,
        -- ove prevista
        * F.Costo * ( 1 - PA.PercEsenzione/100)
    ) as CostoTotale
from
    Patologia PA inner join Terapia T on (
        PA.Nome = T.Patologia
    ) inner join Farmaco F on (
        T.Farmaco = F.NomeCommerciale
    )
    where
        T.DataFineTerapia is not null and
        --  [Turno 2] {
        -- year(T.DataEsordio)       = year(current_date - interval 5 year) and
        -- year(T.DataInizioTerapia) = year(T.DataEsordio) and
        -- year(T.DataFineTerapia)   = year(T.DataEsordio)
        year(T.DataEsordio)       >= 2008 and
        year(T.DataInizioTerapia) >= 2008 and
        year(T.DataFineTerapia)   <= 2010
        -- }
        and not exists (
            select *
            from Esordio E
            where
                E.Paziente = T.Paziente and
                E.Patologia = T.Patologia and
                -- [Turno 2] {
                -- year(E.DataEsordio) < year(T.DataEsordio)
                year(E.DataEsordio) < 2008
                -- }
        )
    group by PA.SettoreMedico
;

/*
 * Esercizio 5 (8 punti)
 * Creare e popolare una tabella INTERVALLO(__CodiceFiscale, Specializzazione__,
 * Giorni) contenente, per ogni paziente, il periodo di tempo medio fra una
 * visita e la successiva della stessa specializzazione, espresso in giorni.
 * [Scrivere poi un trigger che mantenga aggiornata tale tabella.|| A.A. Prec]
 */

-- Ho usato le analytic functions anche se non necessario. L'esercizio poteva
-- esser svolto anche con alcuni join. L'analytic usata è una lag.
delimiter !
drop table if exists INTERVALLO!
create table INTERVALLO (
    CodiceFiscale       char(50),
    Specializzazione    char(50),
    Giorni              int     not null,
    primary key(CodiceFiscale, Specializzazione)
)Engine=InnoDB default charset=latin1!

drop procedure if exists update_intervallo!
create procedure update_intervallo(
    in codfiscale_  char(50),
    in spec_        char(50)
)
begin
    declare updateall tinyint default 0;

    -- Per convenzione se viene passato il valore `null` su uno dei campi verrà
    -- aggiornata l'intera tabella. 
    set updateall = codfiscale_ is null or spec_ is null;
    
    insert into INTERVALLO
    select * from (
        select B.Paziente, B.Specializzazione, avg(Giorni) as Giorni
        from (
            select 
                A.Paziente,
                A.Specializzazione,
                if (
                    @paz = A.Paziente and @spec = A.Specializzazione,
                    datediff(A.`Data`, from_days(@dataprec)) + 
                        least(0, @dataprec := to_days(A.`Data`)),
                    null + least(0, @paz := A.Paziente) +
                        least(0, @spec := A.Specializzazione) +
                        least(0, @dataprec := to_days(A.`Data`))
                )
                as Giorni
            from (
                select *
                from
                    Visita V inner join
                    Medico M on V.Medico = M.Matricola ,
                    (select @spec := '', @paz := '', @dataprec := null) as N
                where 1 = updateall or (
                    V.Paziente = codfiscale_ and
                    M.Specializzazione = spec_
                )
            ) as A
            order by A.Paziente, A.Specializzazione, A.`Data` asc
        ) B
        group by Paziente, Specializzazione
    ) C
    where C.Giorni is not null
    on duplicate key update Giorni = values(Giorni)
    ;
end!

-- Popolo la tabella INTERVALLO :v
call update_intervallo(null, null)!

drop trigger if exists update_intervallo_trigger!
create trigger update_intervallo_trigger
after insert on Visita
for each row
begin
    declare spec_   char(50);

    set spec_ = (
        select Specializzazione
        from Medico
        where Matricola = new.Medico
    );

    -- Aggiorno solo per il paziente e la specializzazione della visita corrente
    -- senza ripopolare tutta la tabella
    call update_intervallo(new.paziente, spec_);
end!
delimiter ;
