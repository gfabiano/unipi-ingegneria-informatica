/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

-- Nei commenti `__` rappresenta la sottolineatura es. __sottolineato__

-- Si consideri la realtà universitaria descritta dalla base di dati relazionale
-- definita dal seguente schema:

-- AULE(__CodAula__, Polo, Piano, Numero)
-- ORARIO(__CodAula, Giorno, Ora__, CodCorso)
-- CORSOLAUREA(__CodLaurea__, Nome, MatrPresidente)
-- CORSO(__CodCorso__, Nome, CodLaurea, Gruppo, Anno, MatricolaDocente)
-- PROPEDEUTICITA(__CodCorsoPrima, CodCorsoDopo__)
-- PROFESSORE(__Matricola__, Nome, Dipartimento)

-- Esprimere le seguenti richieste in linguaggio SQL. Ogni esercizio risolto
-- correttamente assegna 6 punti.

/*
 * Esercizio 1
 * Senza fare uso di viste, indicare per ogni giorno della settimana, il giorno,
 * l'ora e il codice delle aule dove non si svolge lezione.
 */
select B.Giorno, B.Ora, A.CodAula
from Aula A, (
    select distinct O.Giorno, K.Ora
    from Orario O, (select Ora from Orario) as K
) B
where (A.CodAula, B.Giorno, B.Ora) not in (
    select CodAula, Giorno, Ora
    from Orario
);

/*
 * Esercizio 2
 * Indicare il nome dei professori che tengono un numero di ore di lezione
 * inferiore a 5.
 */
select P.Nome
from Professore P inner join 
    Corso C on P.Matricola = C.MatricolaDocente join
    Orario O using(CodCorso)
group by P.Matricola
having count(*) < 5;

/*
 * Esercizio 3
 * Indicare il nome dei professori che sono docenti di almeno un corso, ma non
 * insegnano in alcun corso propedeutico.
 */
select P.Nome
from Corso C inner join Professore P on C.MatricolaDocente = P.Matricola
where P.Matricola not in (
    select MatricolaDocente
    from Corso inner join Propedeuticita on (
        CodCorso = CodCorsoPrima
    )
);

/*
 * Esercizio 4
 * Indicare, per ogni anno e gruppo del corso di laurea in
 * Ingegneria Informatica, l'anno, il gruppo e il numero di corsi attivati
 * (cioè effettivamente in orario).
 */
select C.Anno, C.Gruppo, count(*) as NumeroCorsi
from
    Corso C inner join
    CorsoLaurea CL on C.CodLaurea = CL.CodLaurea
where CL.Nome = 'Ingegneria Informatica'
group by C.Anno, C.Gruppo;

/*
 * Esercizio 5
 * Indicare il nome dei corsi le cui lezioni sono tenute esclusivamente presso
 * il polo B.
 */
select C.Nome
from Corso C
where C.CodCorso not in (
    select CodCorso
    from
        Corso join
        Orario using(CodCorso) join
        Aule using(CodAula)
    where Polo <> 'B'
);
