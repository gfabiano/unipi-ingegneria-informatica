/* 
 * Copyright 2017 Giuseppe Fabiano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Esercizio 1 (10 punti)
 * Considerate le sole visite otorinolaringoiatriche, scrivere una query che
 * restituisca il numero di pazienti, ad oggi maggiorenni, che sono stati
 * visitati solo da otorini di Firenze durante il primo trimestre del 2015.
 */

-- Restituisce i pazienti maggiorenni
create or replace view PazientiMaggiorenni as
select *
from Paziente
where DataNascita + interval 18 year < current_date;

-- Restituisce le visite otorinolaringoiatriche di pazienti maggiorenni nel
-- primo trimestre del 2015
create or replace view VisiteOtorinoMaggiorenni as
select V.Paziente, V.Medico, V.Data, M.Citta
from Visita V inner join
    PazientiMaggiorenni PM on (V.Paziente = PM.CodFiscale) inner join
    Medico M on (V.Medico = M.Matricola)
where year(V.Data) = 2015 and
    month(V.Data) between 1 and 3
;

select count(distinct V.Paziente)
from VisiteOtorinoMaggiorenni V left join
    VisiteOtorinoMaggiorenni V1 on (
        V.Paziente = V1.Paziente and
        V.Citta <> V1.Citta
    )
where V1.Citta is null and V.Citta = 'Firenze'
;

/*
 * Esercizio 2 (10 punti)
 * Implementare una stored function therapy_failures() che riceva in ingresso il
 * codice fiscale di un paziente e il nome commerciale di un farmaco e
 * restituisca, solo se esiste, il settore medico con il più alto numero di
 * terapie iniziate dal paziente nel mese scorso, terminate senza guarigione
 * nello stesso mese.
 */
delimiter !
drop function if exists therapy_failures!

-- Per provare impostare:
--   set year_   = 2012;
--   set month_  = 6;
-- e stampare con:
--   select therapy_failures('uiw1', 'Augmentin');
create function therapy_failures(
    codfiscale_ char(50),
    farmaco_    char(50)
)
returns char(50) not deterministic
begin
    declare month_  int default month(current_date - interval 1 month);
    declare year_   int default year(current_date - interval 1 month);

    return (
        select SettoreMedico from (
            select P.SettoreMedico, count(*) as Terapie
            from Terapia T natural join Esordio E inner join Patologia P on (
                P.Nome = E.Patologia
            )
            where year(T.DataInizioTerapia) = year(T.DataFineTerapia) and
                month(T.DataInizioTerapia) = month(T.DataFineTerapia)
            and (E.DataGuarigione is null or
                E.DataGuarigione > T.DataFineTerapia) and
                year(T.DataInizioTerapia) = year_ and
                month(T.DataInizioTerapia) = month_ and
                T.Farmaco = farmaco_ and
                T.Paziente = codfiscale_
            group by P.SettoreMedico
        ) as A
        order by Terapie desc
        limit 1
    );
end!
delimiter ;

/*
 * Esercizio 3 (13 punti)
 * Con il continuo susseguirsi di nuovi contagi, la direzione della clinica si
 * è resa recentemente disponibile a prestare i suoi dati per analizzare i casi
 * di meningite in pazienti toscani, in particolare, di Pisa e Firenze, che si
 * sono manifestati a partire dal mese di Ottobre 2015, nonostante tali pazienti
 * si fossero sottoposti a vaccinazione con il farmaco Menjugate, nei sei mesi
 * precedenti all'esordio. All'interno del database, le vaccinazioni per una
 * patologia sono registrate come terapie legate a un esordio fittizio del
 * paziente, avvenuto in data 0000-00-00, caratterizzato dalla patologia oggetto
 * di vaccinazione. Per ogni nuovo caso di meningite che coinvolge un paziente
 * della clinica, il database contiene un normale esordio. Nell'analisi dei
 * contagi, per ogni caso di meningite, interessano la città di provenienza del
 * paziente, la data di esordio e il numero di giorni trascorsi dalla
 * vaccinazione. Inoltre, per ogni caso, è importante conoscere il numero medio
 * di giorni trascorsi fra esordio e vaccinazione fino a quel momento,
 * considerando i pazienti della stessa città. Questi dati sono necessari alla
 * casa farmaceutica Novartis (produttrice di Menjugate) per analisi statistiche
 * sui contagi e indagini eziologiche dell'acuirsi della virulenza della
 * patologia.
 *
 * Si richiede di:
 *   i)   creare uno snapshot contenente tutte le informazioni d'interesse per
 *        la casa farmaceutica;
 *   ii)  popolare lo snapshot;
 *   iii) implementare il deferred full refresh a cadenza settimanale.
 */
-- Citta , dataEsordio, numero giorni dalla vaccinazione, media stessa Citta

-- i) creazione snapshots
drop table if exists resoconto_meningite;
drop table if exists resoconto_giorni_meningite;

create table resoconto_meningite(
    Paziente                char(50),
    Citta                   char(50),
    DataEsordio             date,
    GiorniDaVaccinazione    int,
    primary key(Paziente, DataEsordio)
)Engine=InnoDB default charset=latin1;

create table resoconto_giorni_meningite(
    Citta                   char(50),
    MediaGiorni             int
)Engine=InnoDB default charset=latin1;

delimiter !
drop procedure if exists ondemand_meningite!
create procedure ondemand_meningite()
begin
    truncate table resoconto_meningite;
    truncate table resoconto_giorni_meningite;

    insert into resoconto_meningite
    select T.Paziente,
        P.Citta,
        E.DataEsordio,
        datediff(E.DataEsordio, T.DataInizioTerapia)
    from Terapia T inner join Paziente P on (P.CodFiscale = T.Paziente) join
        Esordio E using(Paziente, Patologia)
    where 
        T.DataEsordio < E.DataEsordio and
        T.DataEsordio = '0000-00-00' and
        T.Farmaco = 'Menjugate' and
        E.Patologia = 'Meningite';

    insert into resoconto_giorni_meningite
    select Citta, avg(GiorniDaVaccinazione)
    from resoconto_meningite
    group by Citta;
end!

-- ii) Popolamento snapshot
call ondemand_meningite()!

-- iii) Deferred full refresh
drop event if exists deferred_meningite!
create event deferred_meningite
on schedule every 1 week do
begin
    call ondemand_meningite();
end!
delimiter ;

