# author: Gianluca Cometa

.global _ZN2clC1E3st1
_ZN2clC1E3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $16, %rsp

.set this, -8
.set ss, -16
.set i, -12

# copio parametri
movq %rdi, this(%rbp)
movl %esi, ss(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb ss(%rbp, %rcx), %al
    movb %al, 4(%rdi, %rcx)
    movb %al, 0(%rdi, %rcx)
    
    addb %al, %al
    movsbq %al, %rax
    movq %rax, 8(%rdi, %rcx, 8)
    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1EPc3st1
_ZN2cl5elab1EPc3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $80, %rsp

.set this, -8
.set ar1, -16
.set s2, -24
.set s1, -32
.set i, -20
.set cla, -72
.set return, -80

# copio parametri
movq %rdi, return(%rbp)
movq %rsi, this(%rbp)
movq %rdx, ar1(%rbp)
movl %ecx, s2(%rbp)

movl $0, i(%rbp)

for2:    
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
    movq ar1(%rbp), %r8
    movb (%r8, %rcx), %al
    movb %al, s1(%rbp, %rcx)
    incl i(%rbp)
    jmp for2
finefor2:
# istanzio cla
leaq cla(%rbp), %rdi
movl s1(%rbp), %esi
call _ZN2clC1E3st1

movl $0, i(%rbp)

for3:
    cmpl $4, i(%rbp)
    jge finefor3
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
    movb s2(%rbp, %rcx), %al
    movsbq %al, %rax
    movq %rax, cla+8(%rbp, %rcx, 8)
    incl i(%rbp)
    jmp for3
finefor3:
    movl $0, i(%rbp)
for4:
    cmpl $5, i(%rbp)
    jge finefor4
    movslq i(%rbp), %rcx
    movq return(%rbp), %rbx
    movq cla(%rbp, %rcx, 8), %rax
    movq %rax, (%rbx, %rcx, 8)
    incl i(%rbp)
    jmp for4
finefor4:
    leave
    ret
    
