# auhtor: Gianluca Cometa

.global _ZN2clC1EPc
_ZN2clC1EPc:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set v, -16
.set i, -24

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, v(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq v(%rbp), %r8
    movb (%r8, %rcx), %al
    movb %al, 32(%rdi, %rcx)

    movsbq %al, %rax
    movq %rax, 0(%rdi, %rcx, 8)
    
    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1ER2sti
_ZN2cl5elab1ER2sti:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set ss, -16
.set d, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, ss(%rbp)
movl %edx, d(%rbp)

movl $0, i(%rbp)
for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
if1:
    movq ss(%rbp), %r8
    movq 0(%r8, %rcx, 8), %r9
    movslq d(%rbp), %rbx
    cmpq %r9, %rbx
    jl fineif1
    movq ss(%rbp), %r8
    movb 32(%r8, %rcx, 1), %al
    addb %al, 32(%rdi, %rcx, 1)
    
fineif1:
    movl d(%rbp), %eax
    movl i(%rbp), %ebx
    subl %ebx, %eax
    movslq %eax, %rax
    movq %rax, 0(%rdi, %rcx, 8)
    incl i(%rbp)
    jmp for2
finefor2:
    leave    
    ret
