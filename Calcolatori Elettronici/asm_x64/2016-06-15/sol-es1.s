# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

.global _ZN2clC1EcR3st1
_ZN2clC1EcR3st1:                        # cl::cl(char, st1&)
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-32, %rsp

    movq    %rdi, -8(%rbp)              # this
    movq    %rsi, -16(%rbp)             # c
    movq    %rdx, -24(%rbp)             # &s2

    movq    $0, -32(%rbp)               # i
for1:
    # Test for1
    cmpq    $8, -32(%rbp)
    jge     finefor1
    
    # Corpo for1
    movq    -8(%rbp), %rdi              # this
    movslq  -32(%rbp), %rcx             # i
    movsbq  -16(%rbp), %rax             # c
    addq    %rcx, %rax
    movb    %al, (%rdi, %rcx)           # %al -> s.vc[i]

    incq    -32(%rbp)
    jmp     for1

finefor1:
    movq    $0, -32(%rbp)
for2:
    # Test for2
    movq    -32(%rbp), %rcx             # i
    cmpq    $4, %rcx
    jge     finefor2

    # Corpo for2
    movq    -8(%rbp), %rdi              # this
    movq    -24(%rbp), %rdx             # &s2
    movsbq  (%rdx, %rcx), %rax          # s2.vc[i] -> %rax
    movsbq  (%rdi, %rcx), %r8           # s.vc[i]  -> %r8
    subq    %r8, %rax
    movl    %eax, 8(%rdi, %rcx, 4)      # %eax -> v[i]

    incq    -32(%rbp)
    jmp     for2

finefor2:
    movq    -8(%rbp), %rax
    leave
    ret

.global _ZN2cl5elab1E3st1R3st2
_ZN2cl5elab1E3st1R3st2:                 # cl::elab1(st1, st2&)
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-64, %rsp
    
    movq    %rdi,  -8(%rbp)             # this
    movq    %rsi, -16(%rbp)             # s1
    movq    %rdx, -24(%rbp)             # &s2

    # Calcolo il this di `cla` e lo immetto in %rdi
    leaq    -56(%rbp), %rdi
    movq    $'f', %rsi
    leaq    -16(%rbp), %rdx
    call    _ZN2clC1EcR3st1

    movq    $0, -64(%rbp)
for3:
    # Test for3
    movq    -64(%rbp), %rcx             # i
    cmpq    $4, %rcx
    jge     finefor3

    # Corpo for3
    movq     -8(%rbp), %rdi             # this
    
    # if1
    movb    (%rdi, %rcx), %al           # s.vc[i]-> %al
    cmpb    -16(%rbp, %rcx), %al        # s.vc[1] < s1.vc[i]?
    jge     fineif1
    
    leaq    -56(%rbp), %rdx
    movb    (%rdx, %rcx), %al
    movb    %al, (%rdi, %rcx)

fineif1:
    # if2
    leaq    -56(%rbp), %rdx
    movl    8(%rdx, %rcx, 4), %eax      # cla.s.vc[i] -> %eax
    cmpl    %eax, 8(%rdi, %rcx, 4)      # s.vc[i] < cla.s.vc[i]?
    jge     fineif2
    addl    %ecx, %eax
    movl    %eax, 8(%rdi, %rcx, 4)
fineif2:
    incq    -64(%rbp)
    jmp     for3

finefor3:
    leave
    ret
