# author: Gianluca Cometa

.global _ZN2clC1EcR3st1
_ZN2clC1EcR3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set c, -16
.set s2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movq %rdx, s2(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $8, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb c(%rbp), %al
    addb %cl, %al
    movb %al, (%rdi, %rcx)

    incl i(%rbp)
    jmp for1
finefor1:
    movl $0, i(%rbp)
for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb 0(%rdi, %rcx), %al
    
    movq s2(%rbp), %r8
    movb (%r8, %rcx), %bl
    movsbl %bl, %ebx
    movsbl %al, %eax
    subl %eax, %ebx
    movl %ebx, 8(%rdi, %rcx, 4)
    
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

.global _ZN2cl5elab1E3st1R3st2
_ZN2cl5elab1E3st1R3st2:

#prologo
pushq %rbp
movq %rsp, %rbp
subq $56, %rsp

.set this, -8
.set s1, -16
.set s2, -24
.set cla, -48
.set i, -56

# copio i parametri
movq %rdi, this(%rbp)
movq %rsi, s1(%rbp)
movq %rdx, s2(%rbp)

# istanzio cla
leaq cla(%rbp), %rdi
movb $'f', %sil
leaq s1(%rbp), %rdx
call _ZN2clC1EcR3st1

movl $0, i(%rbp)

for3:
    cmpl $4, i(%rbp)
    jge finefor3
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

if1:
    movb s1(%rbp, %rcx), %al    
    movb (%rdi, %rcx), %bl
    cmpb %bl, %al
    jl if2
    movb cla(%rbp, %rcx), %al
    movb %al, (%rdi, %rcx)
if2:
    movl cla+8(%rbp, %rcx, 4), %eax
    movl 8(%rdi, %rcx, 4), %ebx
    cmpl %ebx, %eax
    jl fineif2
    addl %ecx, %eax
    movl %eax, 8(%rdi, %rcx, 4)
    
fineif2:
    incl i(%rbp)
    jmp for3
finefor3:
    leave
    ret
    
