# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

# Record di attivazione
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1ER3st1
_ZN2clC1ER3st1:
    push    %rbp
    movq    %rsp, %rbp

# %rdi lo riservo per *this, non usarlo MAI per altro
# %rsi lo riservo per *ss, non usarlo MAI per altro
    xor     %rcx, %rcx                  # contatore per eccellenza
for1:
    cmpq    $4, %rcx
    jge     endfor1

    movl    (%rsi, %rcx, 4), %eax
    movb    %al, (%rdi, %rcx)

    movslq  %eax, %rax
    sal     $1, %rax
    movb    %al, 4(%rdi, %rcx)
    sar     $2, %rax 
    movq    %rax, 8(%rdi, %rcx, 8)

    incq    %rcx
    jmp     for1
endfor1:
    movq    %rdi, %rax
    leave
    ret

# Record di attivazione
#           *ar           # -32
#   s1.vc[1] |  s1.vc[0]  # -24
#   s1.vc[3] |  s1.vc[2]  # -16
#          this           #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1E3st1Pi
_ZN2clC1E3st1Pi:
    push    %rbp
    movq    %rsp, %rbp
    addq    $-32, %rsp

    movq    %rdi, -8(%rbp)              # *this
    movq    %rsi, -24(%rbp)             # s1, lower
    movq    %rdx, -16(%rbp)             # s1, upper
    movq    %rcx, -32(%rbp)             # *ar

# %rdi lo riservo per *this, non usarlo MAI per altro

    xor     %rcx, %rcx                  # contatore per eccellenza
for2:
    cmpq    $4, %rcx
    jge     endfor2

    leaq    -24(%rbp), %rsi
    movl    (%rsi, %rcx, 4), %eax
    movb    %al, (%rdi, %rcx)

    movslq  %eax, %rax

    sar     $2, %al 
    movq    %rax, 8(%rdi, %rcx, 8)

    movq    -32(%rbp), %rsi
    movslq  (%rsi, %rcx, 4), %rax
    sal     $1, %rax
    movb    %al, 4(%rdi, %rcx)

    incq    %rcx
    jmp     for2
endfor2:
    movq    %rdi, %rax
    leave
    ret


# Record di attivazione
#     v3     |    v1      # -96
#           v2[0]         # -88
#           v2[1]         # -80
#           v2[2]         # -72
#           v2[3]         # -64
#    s1[1]   |   s1[0]    # -56
#    s1[4]   |   s1[3]    # -40
#            |    s2      # -32
#          *ar1           # -24
#          *this          # -16
#           ret           #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2cl5elab1EPc3st2
_ZN2cl5elab1EPc3st2:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-96, %rsp                  # allineamento 16

    movq    %rdi, -8(%rbp)              # *ret
    movq    %rsi, -16(%rbp)             # *this
    movq    %rdx, -24(%rbp)             # *ar1
    movq    %rcx, -32(%rbp)             # s2 lower

    # -56 // s1
    # -96 // cla
    xor     %rcx, %rcx
for3:
    cmpq    $4, %rcx
    jge     endif3

    movq    -24(%rbp), %rdx
    movsbl  (%rdx, %rcx), %eax
    addl    %ecx, %eax

    movl    %eax, -56(%rbp, %rcx, 4)

    incq    %rcx
    jmp     for3
endif3:

    leaq    -96(%rbp), %rdi
    leaq    -56(%rbp), %rsi
    call    _ZN2clC1ER3st1

    xor     %rcx, %rcx
for4:
    cmpq    $4, %rcx
    jge     endfor4

    movb    -32(%rbp, %rcx), %al
    movb    %al, -96+4(%rbp, %rcx)

    incq    %rcx
    jmp     for4
endfor4:
    # Sposto ret in %rax
    movq    -8(%rbp), %rax

    # Copio l'oggetto cla per valore
    movq    $5, %rcx
    movq    %rax, %rdi
    leaq    -96(%rbp), %rsi
    rep     movsq

    leave
    ret
