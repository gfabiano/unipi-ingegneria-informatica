# author: Gianluca Cometa

.global _ZN2clC1E3st1
_ZN2clC1E3st1:

# prologo
pushq %rbp
movq %rsp, %Rbp
subq $16, %rsp

.set this, -8
.set ss, -16
.set i, -12

# copio parametri
movq %rdi, this(%rbp)
movl %esi, ss(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb ss(%rbp, %rcx), %al
    movb %al, (%rdi, %rcx)

    movsbq %al, %rax
    movq %rax, 24(%rdi, %rcx, 8)

    movb ss(%rbp, %rcx), %bl
    shlb $2, %bl
    movsbl %bl, %ebx
    movl %ebx, 4(%rdi, %rcx, 4)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2clC1ER3st1Pi
_ZN2clC1ER3st1Pi:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set s1, -16
.set ar2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, s1(%rbp)
movq %rdx, ar2(%rbp)

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq s1(%rbp), %r8
    movb (%r8, %rcx), %al
    movb %al, (%rdi, %rcx)

    movsbq %al, %rax
    negq %rax
    movq %rax, 24(%rdi, %rcx, 8)

    movq ar2(%rbp), %r9
    movl (%r9, %rcx, 4), %ebx
    movl %ebx, 4(%rdi, %rcx, 4)

    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

.global _ZN2cl5elab1EPcRK3st2
_ZN2cl5elab1EPcRK3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $96, %rsp

.set this, -8
.set ar1, -16
.set s2, -24
.set s1, -32
.set i, -28
.set cla, -88
.set return, -96

# copio parametri
movq %rdi, return(%rbp)
movq %rsi, this(%rbp)
movq %rdx, ar1(%rbp)
movq %rcx, s2(%rbp)

movl $0, i(%rbp)

for3:    
    cmpl $4, i(%rbp)
    jge finefor3
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq ar1(%rbp), %r8
    movb (%r8, %rcx), %al
    subb %cl, %al
    movb %al, s1(%rbp, %rcx, 1)

    incl i(%rbp)
    jmp for3

finefor3:
# istanzio cla
leaq cla(%rbp), %rdi
movl s1(%rbp), %esi
call _ZN2clC1E3st1

movl $0, i(%rbp)

for4:
    cmpl $4, i(%rbp)
    jge finefor4
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq s2(%rbp), %r9
    movq (%r9, %rcx, 8), %rax
    movl %eax, cla+4(%rbp, %rcx, 4)
    
    incl i(%rbp)
    jmp for4
finefor4:
    movl $0, i(%rbp)
for5:
    cmpl $7, i(%rbp)
    jge finefor5
    movslq i(%rbp), %rcx
    movq return(%rbp), %rbx
    movq cla(%rbp, %rcx, 8), %rax
    movq %rax, (%rbx, %rcx, 8)
    
    incl i(%rbp)
    jmp for5
finefor5:
    leave
    ret
