# author: Gianluca Cometa

.global _ZN2clC1EcR3st2
_ZN2clC1EcR3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set c, -16
.set s2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movq %rdx, s2(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
    movb c(%rbp), %al
    addb %cl, %al
    movb %al, 0(%rdi, %rcx)

    movb 0(%rdi, %rcx), %al
    movq s2(%rbp), %r8
    movl (%r8, %rcx, 4), %ebx
    movsbl %al, %eax
    addl %eax, %ebx
    movslq %ebx, %rbx
    movq %rbx, 8(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

