# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

# Record di attivazione
#  ss.vi[1]  | ss.vi[0]   # -16
#  ss.vi[4]  | ss.vi[3]   #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1E3st1
_ZN2clC1E3st1:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-16, %rsp                  # leaf function

    # %rdi lo riservo per *this, non usarlo MAI per altro

    movq    %rsi, -16(%rbp)             # ss(lower)
    movq    %rdx, -8(%rbp)              # ss(upper)

    xor     %rcx, %rcx
for1:
    cmpq    $4, %rcx
    jge     endfor1

    movslq  -16(%rbp, %rcx, 4), %rax
    movb    %al, 32(%rdi, %rcx)         # v1[i] = s1.vi[i]

    sal     $1 ,%rax                    # ss.vi[i] * 2
    movq    %rax, (%rdi, %rcx, 8)       # v2[i] = ss.vi[i] * 2
    movb    %al, 36(%rdi, %rcx)         # v3[i] = ss.vi[i] * 2

    incq    %rcx
    jmp     for1
endfor1:
    movq    %rdi, %rax
    leave
    ret

# Record di attivazione
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1ER3st1Pi
_ZN2clC1ER3st1Pi:
    pushq   %rbp
    movq    %rsp, %rbp

    # %rdi lo riservo per *this, non usarlo MAI per altro
    # %rsi lo riservo per *s1, non usarlo MAI per altro
    # %rdx lo riservo per *ar2, non usarlo MAI per altro

    xor     %rcx, %rcx
for2:
    cmpq    $4, %rcx
    jge     endfor2

    movslq  (%rsi, %rcx, 4), %rax
    movb    %al, 32(%rdi, %rcx)         # v1[i] = s1.vi[i]

    sal     $2, %rax                    # s1.vi[i] * 4
    movq    %rax, (%rdi, %rcx, 8)       # v2[i] = s1.vi[i] * 4

    movl    (%rdx, %rcx, 4), %eax
    movb    %al, 36(%rdi, %rcx)         # v3[i] = ae2[i]

    incq    %rcx
    jmp     for2
endfor2:
    movq    %rdi, %rax
    leave
    ret


# Record di attivazione
#           v2[0]         # -96
#           v2[1]         # -88
#           v2[2]         # -80
#           v2[3]         # -72
#     v3     |    v1      # -64
#    s1[1]   |   s1[0]    # -56
#    s1[4]   |   s1[3]    # -40
#            |    s2      # -32
#          *ar1           # -24
#          *this          # -16
#           ret           #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2cl5elab1EPc3st2
_ZN2cl5elab1EPc3st2:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-96, %rsp                  # allineamento 16

    movq    %rdi, -8(%rbp)              # *ret
    movq    %rsi, -16(%rbp)             # *this
    movq    %rdx, -24(%rbp)             # *ar1
    movq    %rcx, -32(%rbp)             # s2 lower

    # -56 // s1
    # -96 // cla
    xor     %rcx, %rcx
for3:
    cmpq    $4, %rcx
    jge     endif3

    movq    -24(%rbp), %rdx
    movsbl  (%rdx, %rcx), %eax          # %eax = ar1[i]
    addl    %ecx, %eax                  # %eax = ar1[i] + i
    movl    %eax, -56(%rbp, %rcx, 4)    # s1.vi[i] = %eax

    incq    %rcx
    jmp     for3
endif3:

    leaq    -96(%rbp), %rdi             # address of cla
    movq    -56(%rbp), %rsi             # s1(lower)
    movq    -56+8(%rbp), %rdx           # s1(upper)
    call    _ZN2clC1E3st1

    xor     %rcx, %rcx
for4:
    cmpq    $4, %rcx
    jge     endfor4

    movb    -32(%rbp, %rcx), %al        # %al = s2.vd[i]
    movb    %al, -96+36(%rbp, %rcx)     # cla.v3[i] = %al

    incq    %rcx
    jmp     for4
endfor4:
    # Sposto ret in %rax
    movq    -8(%rbp), %rax

    # Copio l'oggetto cla per valore
    movq    $5, %rcx
    movq    %rax, %rdi
    leaq    -96(%rbp), %rsi
    rep     movsq

    leave
    ret
