# author: Gianluca Cometa

.global _ZN2clC1E3st1
_ZN2clC1E3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

# copio parametri
movq %rdi, -8(%rbp)
movq %rsi, -24(%rbp)
movq %rdx, -16(%rbp)

movl $0, -32(%rbp)

for1:
    cmpl $4, -32(%rbp)
    jge finefor1
    movslq -32(%rbp), %rcx
    movq -8(%rbp), %rdi
    
    movl -24(%rbp, %rcx, 4), %eax
    movb %al, 32(%rdi, %rcx, 1)
    
    shll $1, %eax
    movslq %eax, %rax
    movq %rax, 0(%rdi, %rcx, 8)
    movb %al, 36(%rdi, %rcx, 1)

    incl -32(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2clC1ER3st1Pi
_ZN2clC1ER3st1Pi:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

# copio parametri
movq %rdi, -8(%rbp)
movq %rsi, -16(%rbp)
movq %rdx, -24(%rbp)

movl $0, -32(%rbp)

for2:
    cmpl $4, -32(%rbp)
    jge finefor2
    movslq -32(%rbp), %rcx
    movq -8(%rbp), %rdi
    
    movq -16(%rbp), %r8
    movl (%r8, %rcx, 4), %eax
    movb %al, 32(%rdi, %rcx, 1)

    shll $2, %eax
    movslq %eax, %rax
    movq %rax, 0(%rdi, %rcx, 8)

    movq -24(%rbp), %r9
    movl (%r9, %rcx, 4), %ebx
    movb %bl, 36(%rdi, %rcx, 1)
    
    incl -32(%rbp)
    jmp for2
finefor2:
    leave
    ret

.global _ZN2cl5elab1EPc3st2
_ZN2cl5elab1EPc3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $88, %rsp

# copio parametri
movq %rdi, -88(%rbp)
movq %rsi, -8(%rbp)
movq %rdx, -16(%rbp)
movl %ecx, -24(%rbp)

movl $0, -28(%rbp)

for3:
    cmpl $4, -28(%rbp)
    jge finefor3
    movslq -28(%rbp), %rcx
    movq -8(%rbp), %rdi
    
    movq -16(%rbp), %r8
    movb (%r8, %rcx), %al
    movsbl %al, %eax
    addl %ecx, %eax
    movl %eax, -40(%rbp, %rcx, 4)
    incl -28(%rbp)
    jmp for3
finefor3:
# istanzio cla
leaq -80(%rbp), %rdi
movq -40(%rbp), %rsi
movq -32(%rbp), %rdx
call _ZN2clC1E3st1

movl $0, -28(%rbp)
for4:
    cmpl $4, -28(%rbp)
    jge finefor4
    movslq -28(%rbp), %rcx
    movq -8(%rbp), %rdi
    
    movb -24(%rbp, %rcx, 1), %al
    movb %al, -52(%rbp, %rcx, 1)
    incl -28(%rbp)
    jmp for4
finefor4:
movl $0, -28(%rbp)
for5:
    cmpl $6, -28(%rbp)
    jge finefor5
    movslq -28(%rbp), %rcx
    movq -88(%rbp), %rbx
    movq -80(%rbp, %rcx, 8), %rax
    movq %rax, (%rbx, %rcx, 8)

    incl -28(%rbp)
    jmp for5
finefor5:
    leave
    ret
    

