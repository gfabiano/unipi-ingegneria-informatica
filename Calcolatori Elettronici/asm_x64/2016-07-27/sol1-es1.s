# author: Gianluca Cometa

.global _ZN2cl
_ZN2cl:
    ret

.global _ZN2clC1EPc
_ZN2clC1EPc:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set v, -16
.set i, -24

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, v(%rbp)

movl $0, i(%rbp)

# corpo
movq v(%rbp), %r8
movb (%r8), %al
movb %al, 0(%rdi)
incb (%r8)

movb 1(%r8), %bl
movb %bl, 1(%rdi)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
    movq v(%rbp), %r8
    movb (%r8, %rcx), %al
    addb 0(%rdi), %al
    movb %al, 8(%rdi, %rcx)

    movb (%r8, %rcx), %bl
    addb 1(%rdi), %bl
    movsbq %bl, %rbx
    movq %rbx, 16(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1ER2sti
_ZN2cl5elab1ER2sti:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set d, -16
.set i, -12
.set ss, -24

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, ss(%rbp)
movl %edx, d(%rbp)

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

if1:
    movq ss(%rbp), %r8
    movq 8(%r8, %rcx, 8), %r9
    movslq d(%rbp), %rax
    cmpq %rax, %r9
    ja fineif1
    movb 0(%r8, %rcx), %al
    addb %al, 8(%rdi, %rcx)
fineif1:
    movb (%rdi), %al
    movl d(%rbp), %ebx
    movsbq %al, %rax
    movslq %ebx, %rbx
    addq %rax, %rbx
    movq %rbx, 16(%rdi, %rcx, 8)
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret
