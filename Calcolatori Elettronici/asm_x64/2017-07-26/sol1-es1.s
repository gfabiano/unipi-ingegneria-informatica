# author: Gianluca Cometa

.global _ZN2clC1E
_ZN2clC1E:
    ret

.global _ZN2clC1EPc
_ZN2clC1EPc:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set v, -16
.set i, -24

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, v(%rbp)

movq v(%rbp), %r8
movb 0(%r8), %al
movb %al, (%rdi)
incb 0(%r8)

movb 1(%r8), %bl
movb %bl, 1(%rdi)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb (%r8, %rcx), %al
    movb 0(%rdi), %bl
    addb %al, %bl
    movb %bl, 40(%rdi, %rcx)
    movb 1(%rdi), %dl
    addb %al, %dl
    movsbq %dl, %rdx
    movq %rdx, 8(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret
