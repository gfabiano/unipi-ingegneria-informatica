# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

.global _ZN2clEv
_ZN2clEv:
    movq    %rdi, %rax
    ret

.global _ZN2clC1EPc
_ZN2clC1EPc:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-16, %rsp

    movq    %rdi, -8(%rbp)
    movq    %rsi, -16(%rbp)

    movb    (%rsi), %al                 # v[0]
    incb    (%rsi)                      # v[0]++
    movb    %al, 40(%rdi)               # a = v[0]
    movb    1(%rsi), %al                # v[1]
    movb    %al, 41(%rdi)               # b = v[1]

    xor     %rcx, %rcx
for1:
    cmpq    $4, %rcx
    jge     endfor1

    movq    -8(%rbp), %rdi
    movq    -16(%rbp), %rsi

    movsbl  (%rsi, %rcx), %eax          # v[i]
    movslq  %eax, %r8
    addb    40(%rdi), %al               # v[i] + a
    movb    %al, 32(%rdi, %rcx)         # s.vv1[i] = v[i] + a

    addb    41(%rdi), %r8b              # v[i] + b
    movq    %r8, (%rdi, %rcx, 8)        # s.vv2[i] = v[i] + b

    incq    %rcx
    jmp     for1
endfor1:
    movq    -8(%rbp), %rax
    leave
    ret

.global _ZN2cl5elab1ER2sti
_ZN2cl5elab1ER2sti:
    pushq   %rbp
    movq    %rsp, %rbp

    # %rdi *this  ; non modificare
    # %rsi *ss    ; non modificare
    # %rdx d      ; non modificare

    movslq  %edx, %rdx                  # estendo d a 64bit

    xor     %rcx, %rcx                  # azzero %rcx
for2:
    cmpq    $4, %rcx
    jge     endfor2

    cmpq    (%rsi, %rcx, 8), %rdx       # d >= ss.vv2[i]
    jl      endif

    movb    32(%rsi, %rcx), %al         # ss.vv1[i]
    addb    %al, 32(%rdi, %rcx)         # s.vv1[i] += ss.vv1[i];
endif:
    movsbl  40(%rdi), %eax              # a
    movl    %ecx, %r8d                  # i
    addl    %edx, %eax                  # a + d
    subl    %r8d, %eax                  # (a + d) - i

    movslq  %eax, %rax                  # estendo a 64 bit il risultato
    movq    %rax, (%rdi, %rcx, 8)       # s.vv2[i] = %rax;

    incq    %rcx
    jmp     for2
endfor2:
    leave
    ret
