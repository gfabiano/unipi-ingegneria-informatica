# author: Gianluca Cometa

.global _ZN2clC1Ec3st1
_ZN2clC1Ec3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $24, %rsp

.set this, -8
.set c, -16
.set s2, -24
.set i, -20

# copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movl %edx, s2(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
    movb c(%rbp), %al
    movb %al, 0(%rdi, %rcx, 1)
    
    movb s2(%rbp, %rcx, 1), %bl
    subb %al, %bl
    movsbq %bl, %rbx
    movq %rbx, 8(%rdi, %rcx, 8)
    
    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1ER3st1
_ZN2cl5elab1ER3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $64, %rsp

.set this, -8
.set s1, -16
.set cla, -56
.set i, -64

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, s1(%rbp)

# istanzio cla
leaq cla(%rbp), %rdi
movb $'p', %sil
movq s1(%rbp), %r8
movl (%r8), %edx
call _ZN2clC1Ec3st1

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
    
if1:
    movq s1(%rbp), %r8
    movb (%r8, %rcx), %al    
    movb 0(%rdi, %rcx), %bl    
    cmpb %al, %bl
    ja fineif1
    movb cla(%rbp, %rcx), %al
    movb %al, 0(%rdi, %rcx)

    movq cla+8(%rbp, %rcx, 8), %r9
    addq %rcx, %r9
    movq %r9, 8(%rdi, %rcx, 8)
fineif1:
    incl i(%rbp)
    jmp for2
finefor2:    
    leave
    ret    
    
