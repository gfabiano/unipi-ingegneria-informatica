# author: Gianluca Cometa

.global _ZN2clC1Ec3st1
_ZN2clC1Ec3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set c, -16
.set s2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movq %rdx, s2(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $8, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb c(%rbp), %al
    addb %cl, %al
    movb %al, 0(%rdi, %rcx)

    incl i(%rbp)
    jmp for1
finefor1:
    movl $0, i(%rbp)
for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb (%rdi, %rcx), %al
    movb s2(%rbp, %rcx), %bl    
    subb %al, %bl
    movsbq %bl, %rbx
    movq %rbx, 8(%rdi, %rcx, 8)
    
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

.global _ZN2cl5elab1ER3st13st2
_ZN2cl5elab1ER3st13st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $80, %rsp

.set this, -8
.set s1, -16
.set s2, -32
.set cla, -72
.set i, -80

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, s1(%rbp)
movq %rdx, s2(%rbp)

# istanzio cla
leaq cla(%rbp), %rdi
movb $'S', %sil
movq s1(%rbp), %r8
movq (%r8), %rdx
call _ZN2clC1Ec3st1

movl $0, i(%rbp)

for3:
    cmpl $4, i(%rbp)
    jge finefor3
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

if1:
    movq s1(%rbp), %r8
    movb (%r8, %rcx), %al
    movb (%rdi, %rcx), %bl    
    cmpb %al, %bl    
    ja if2
    movb cla(%rbp, %rcx), %al
    movb %al, (%rdi, %rcx)
if2:
    movq cla+8(%rbp, %rcx, 8), %rax
    movq 8(%rdi, %rcx, 8), %rbx
    cmpq %rbx, %rax
    ja fineif2
    addq %rcx, %rax
    movq %rax, 8(%rdi, %rcx, 8)

fineif2:
    incl i(%rbp)
    jmp for3
finefor3:
    leave
    ret
