# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

# Record di attivazione
#            i            # -40
#   s2.vc[1] |  s2.vc[0]  # -32
#~~~~~~~~~~~~|  s2.vc[2]  # -24
#           *c            # -16
#          this           #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1EPc3st2
_ZN2clC1EPc3st2: # cl::cl(char *c, st2 s2)
    pushq   %rbp
    movq    %rsp, %rbp
    subq    $48, %rsp

    movq    %rdi,  -8(%rbp)             # this
    movq    %rsi, -16(%rbp)             # *c
    movq    %rdx, -32(%rbp)             # s2, parte + significativa
    movq    %rcx, -24(%rbp)             # s2  parte - significativa

    movq    $0, -40(%rbp)               # i = 0
for1:
    movq    -40(%rbp), %rcx             # i
    cmpq    $4, %rcx                    # i < 4?
    jge     finefor1

    movq    -8(%rbp), %rdi              # this
    movq    -16(%rbp), %rsi             # *c
    movb    (%rsi, %rcx), %al           # c[i] -> %al
    movb    %al, (%rdi, %rcx)           # %al -> s.vc[i]
    movsbl  %al, %eax                   # estendo %al
    addl    -32(%rbp, %rcx, 4), %eax    # s2.vd[i] + s.vc[i] -> %eax
    movslq  %eax, %rax                  # estendo %eax a 64b
    movq    %rax, 8(%rdi, %rcx, 8)      # v[i] = %rax, offset 8 causa allineam.

    incq    -40(%rbp)
    jmp for1

finefor1:
    movq    -8(%rbp), %rax
    leave
    ret

# Record di attivazione
#            i            # -80
# ~~~~~~~~~~~|cla.s.vc[i] # -72
#        cla.v[0]         # -64
#        cla.v[1]         # -56
#        cla.v[2]         # -48
#        cla.v[3]         # -40
#  s2.vc[1]  |  s2.vc[0]  # -32
# ~~~~~~~~~~~|  s2.vc[2]  # -24
#   s1.vc[1] |  s1.vc[0]  # -16
#          this           #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2cl5elab1E3st13st2
_ZN2cl5elab1E3st13st2:
    pushq   %rbp
    movq    %rsp, %rbp
    subq    $80, %rsp

    movq    %rdi,  -8(%rbp)             # this
    movq    %rsi, -16(%rbp)             # s1
    movq    %rdx, -32(%rbp)             # s2, parte + significativa
    movq    %rcx, -24(%rbp)             # s2, parte - significativa

    leaq    -72(%rbp), %rdi
    leaq    -16(%rbp), %rsi
    movq    -32(%rbp), %rdx
    movq    -24(%rbp), %rcx
    call    _ZN2clC1EPc3st2

    movq    $0, -80(%rbp)               # i = 0
for2:
    movq    -80(%rbp), %rcx
    cmpq    $4, %rcx
    jge     finefor2

    movq     -8(%rbp), %rdi             # this

    # if1
    movb    (%rdi, %rcx), %al           # s.vc[i] -> %al
    cmpb    -16(%rbp, %rcx), %al        # s.vc[i] < s1.vc[i]
    jge     fineif1
    movb    -72(%rbp, %rcx), %al        # cla.s.vc[i] -> %al
    movb    %al, (%rdi, %rcx)           # %al -> s.vc[i]
fineif1:
    # if2
    movq    -72+8(%rbp, %rcx, 8), %rax  # cla.v[i] -> %rax
    cmpq    %rax, 8(%rdi, %rcx, 8)      # v[i] <= cla.v[i]
    jg      fineif2
    subq    %rax, 8(%rdi, %rcx, 8)      # v[i] -= cla.v[i]

fineif2:
    incq    -80(%rbp)
    jmp for2
finefor2:
    leave
    ret
