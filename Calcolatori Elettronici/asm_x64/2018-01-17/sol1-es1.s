# author: Gianluca Cometa

.global _ZN2clC1EPc3st2
_ZN2clC1EPc3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $40, %rsp

# copio parametri
movq %rdi, -8(%rbp)
movq %rsi, -16(%rbp)
movq %rdx, -32(%rbp)
movq %rcx, -24(%rbp)

movl $0, -40(%rbp)

for1:
    cmpl $4, -40(%rbp)
    jge finefor1
    movslq -40(%rbp), %rcx
    movq -8(%rbp), %rdi
    
    movq -16(%rbp), %r8
    movb (%r8, %rcx), %al
    movb %al, 0(%rdi, %rcx)

    movsbl %al, %eax
    movl -32(%rbp, %rcx, 4), %ebx
    addl %eax, %ebx
    movslq %eabx, %rbx
    movq %rbx, 8(%rdi, %rcx, 8)
    incl -40(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1E3st13st2
_ZN2cl5elab1E3st13st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $72, %rsp

# copio parametri
movq %rdi, -8(%rbp)
movl %esi, -16(%rbp)
movq %rdx, -32(%rbp)
movq %rcx, -24(%rbp)

# istanzio cla
leaq -72(%rbp), %rdi
leaq -16(%rbp), %rsi
movq -32(%rbp), %rdx
movq -24(%rbp), %rcx
call _ZN2clC1EPc3st2

movl $0, -12(%rbp)

for2:
    cmpl $4, -12(%rbp)
    jge finefor2
    movslq -12(%rbp), %rcx
    movq -8(%rbp), %rdi
    
if2:
    movb -16(%rbp, %rcx), %al
    movb 0(%rdi, %rcx), %bl
    cmpb %al, %bl
    ja if3
    movb -72(%rbp, %rcx), %al
    movb %al, 0(%rdi, %rcx)
if3:
    movq -64(%rbp, %rcx, 8), %rax
    movq 8(%rdi, %rcx, 8), %rbx
    cmpq %rax, %rbx
    ja fineif3
    subq %rax, 8(%rdi, %rcx, 8)
fineif3:
    incl -12(%rbp)
    jmp for2
finefor2:
    leave
    ret

