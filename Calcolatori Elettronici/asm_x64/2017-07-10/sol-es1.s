# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

.global _ZN2clC1E3st1
_ZN2clC1E3st1:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-16, %rsp

    movq    %rdi, -8(%rbp)
    movq    %rsi, -16(%rbp)

    xor     %rcx, %rcx
for1:
    cmpq    $4, %rcx
    jge     endfor1

    movsbl  -16(%rbp, %rcx), %eax
    movb    %al, 4(%rdi, %rcx)
    movb    %al, (%rdi, %rcx)

    sal     %eax
    movslq  %eax, %rax

    movq    %rax, 8(%rdi, %rcx, 8)

    incq    %rcx
    jmp     for1
endfor1:
    movq    %rdi, %rax
    leave
    ret


.global _ZN2clC1E3st1Pl
_ZN2clC1E3st1Pl:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-16, %rsp

    movq    %rdi, -8(%rbp)
    movq    %rsi, -16(%rbp)
    movq    %rdx, -24(%rbp)

    xor     %rcx, %rcx
for2:
    cmpq    $4, %rcx
    jge     endfor2

    movsbl  -16(%rbp, %rcx), %eax
    movb    %al, 4(%rdi, %rcx)
    movb    %al, (%rdi, %rcx)

    movq    (%rdx, %rcx, 8), %rax
    movq    %rax, 8(%rdi, %rcx, 8)

    incq    %rcx
    jmp     for2
endfor2:
    movq    %rdi, %rax
    leave
    ret

.global _ZN2cl5elab1EPc3st2
_ZN2cl5elab1EPc3st2:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-96, %rsp

    movq    %rdi, -8(%rbp)              # *ret
    movq    %rsi, -16(%rbp)             # *this
    movq    %rdx, -24(%rbp)             # *ar1
    movq    %rcx, -48(%rbp)             # s2 (lower)
    movq    %r8, -40(%rbp)              # s2 (upper)

    # -56, s1
    # -96, cla
    xor     %rcx, %rcx
for3:
    cmpq    $4, %rcx
    jge     endfor3

    movb    (%rdx, %rcx), %al
    movb    %al, -56(%rbp, %rcx)        # s1.vi[i] = ar1[i];

    incq    %rcx
    jmp     for3
endfor3:
    leaq    -96(%rbp), %rdi             # address of cla
    movq    -56(%rbp), %rsi             # s1
    call    _ZN2clC1E3st1               # call cl::cl(st1 ss)

    xor     %rcx, %rcx
for4:
    cmpq    $4, %rcx
    jge     endfor4

    movslq  -48(%rbp, %rcx, 4), %rax    # s2.vd[i];
    movq    %rax, -96+8(%rbp, %rcx, 8)  # cla.v3[i] = s2.vd[i];

    incq    %rcx
    jmp     for4
endfor4:
    movq    -8(%rbp), %rax

    movq    $5, %rcx
    movq    %rax, %rdi
    leaq    -96(%rbp), %rsi
    rep     movsq

    leave
    ret


