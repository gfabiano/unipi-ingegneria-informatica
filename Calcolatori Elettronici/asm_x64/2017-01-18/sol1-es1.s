#author: Gianluca Cometa

.global _ZN2clC1Ec3st2
_ZN2clC1Ec3st2:

#prologo
pushq %rbp
movq %rsp, %rbp
subq $40, %rsp

.set this, -8
.set c, -16
.set s2, -32
.set i, -40

#copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movq %rdx, s2(%rbp)
movq %rcx, s2+8(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb c(%rbp), %al
    movb %al, (%rdi, %rcx)

    movb 0(%rdi, %rcx), %bl
    movl s2(%rbp, %rcx, 4), %eax
    movsbl %bl, %ebx
    subl %ebx, %eax
    movslq %eax, %rax
    movq %rax, 8(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1E3st1R3st2
_ZN2cl5elab1E3st1R3st2:

#prologo
pushq %rbp
movq %rsp, %rbp
subq $64, %rsp

.set this, -8
.set s1, -16
.set i, -12
.set s2, -24
.set cla, -64

#copio parametri
movq %rdi, this(%rbp)
movl %esi, s1(%rbp)
movq %rdx, s2(%rbp)

#istanzio cla
leaq cla(%rbp), %rdi
movb $'f', %sil
movq s2(%rbp), %r8
movq (%r8), %rdx
movq 8(%r8), %rcx
call _ZN2clC1Ec3st2

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi
if1:
    movb s1(%rbp, %rcx), %al
    movb 0(%rdi, %rcx), %bl
    cmpb %bl, %al
    jl if2
    movb cla(%rbp, %rcx), %al
    movb %al, (%rdi, %rcx)
if2:
    movq cla+8(%rbp, %rcx, 8), %r8
    movq 8(%rdi, %rcx, 8), %r9
    cmpq %r9, %r8
    jle fineif2
    addq %r8, 8(%rdi, %rcx, 8)
fineif2:
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

