# author: Gianluca Cometa

.global _ZN2clC1E3st1
_ZN2clC1E3st1:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set ss, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, ss(%rbp)
movq %rdx, ss+8(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movl ss(%rbp, %rcx, 4), %eax
    movb %al, 0(%rdi, %rcx)

    shrl $1, %eax    
    movslq %eax, %rax
    movq %rax, 8(%rdi, %rcx, 8)

    movl ss(%rbp, %rcx, 4), %ebx
    shll $1, %ebx
    movb %bl, 4(%rdi, %rcx)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2clC1ER3st1Pi
_ZN2clC1ER3st1Pi:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set s1, -16
.set ar2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, s1(%rbp)
movq %rdx, ar2(%rbp)

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq s1(%rbp), %r8
    movl (%r8, %rcx, 4), %eax
    movb %al, (%rdi, %rcx)

    shrl $2, %eax
    movslq %eax, %rax
    movq %rax, 8(%rdi, %rcx, 8)

    movq ar2(%rbp), %r9
    movl (%r9, %rcx, 4), %ebx
    movb %bl, 4(%rdi, %rcx)

    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

.global _ZN2cl5elab1EPc3st2
_ZN2cl5elab1EPc3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $88, %rsp

.set this, -8
.set s2, -16
.set i, -12
.set ar1, -24
.set s1, -40
.set cla, -80
.set return, -88

# copio parametri
movq %rdi, return(%rbp)
movq %rsi, this(%rbp)
movq %rdx, ar1(%rbp)
movl %ecx, s2(%rbp)

movl $0, i(%rbp)

for3:
    cmpl $4, i(%rbp)
    jge finefor3
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq ar1(%rbp), %r8
    movb (%r8, %rcx, 1), %al
    addb %cl, %al
    movsbl %al, %eax
    movl %eax, s1(%rbp, %rcx, 4)

    incl i(%rbp)
    jmp for3
finefor3:
# istanzio cla
leaq cla(%rbp), %rdi
movq s1(%rbp), %rsi
movq s1+8(%rbp), %rdx
call _ZN2clC1E3st1

movl $0, i(%rbp)

for4:
    cmpl $4, i(%rbp)
    jge finefor4
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb s2(%rbp, %rcx, 1), %al
    movb %al, cla+4(%rbp, %rcx, 1)
    
    incl i(%rbp)
    jmp for4
finefor4:
    movl $0, i(%rbp)
for5:
    cmpl $5, i(%rbp)
    jge finefor5
    movslq i(%rbp), %rcx
    movq return(%rbp), %rbx
    movq cla(%rbp, %rcx, 8), %rax
    movq %rax, (%rbx, %rcx, 8)

    incl i(%rbp)
    jmp for5
finefor5:
    leave
    ret
