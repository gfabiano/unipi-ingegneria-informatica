# Copyright 2019 Anonymous
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# reviewed by Giuseppe Fabiano

.text

.global _ZN2clC1Ec3st1
_ZN2clC1Ec3st1: 
    pushq   %rbp
    movq    %rsp,%rbp
    subq    $32, %rsp

    movq    %rdi,-8(%rbp)
    movq    %rsi,-16(%rbp)
    movq    %rdx,-24(%rbp)
    
    movl    $0,-32(%rbp)
    
for: 
    movslq  -32(%rbp),%rcx
    cmpq    $8,%rcx
    jl      corpo
    jmp     continua

corpo:
    movb    -16(%rbp),%al
    addb    %cl,%al
    movq    -8(%rbp),%rdx
    movb    %al,(%rdx,%rcx)

    incl    -32(%rbp)
    jmp     for

continua:
    movl    $0,-32(%rbp)

for2:
    movslq  -32(%rbp),%rcx
    cmpq    $4,%rcx
    jl      corpo2
    jmp     fine

corpo2:
    movq    -8(%rbp),%rdx
    movb    (%rdx,%rcx),%bl
    movb    -24(%rbp,%rcx),%al
    subb    %bl,%al
    movsbl  %al,%eax
    movslq  %eax,%rax
    movq    %rax,8(%rdx,%rcx,8)

    incl    -32(%rbp)
    jmp     for2

fine:
    movq    -8(%rbp),%rax
    leave 
    ret

.global _ZN2cl5elab1ER3st13st2
_ZN2cl5elab1ER3st13st2:
    pushq   %rbp
    movq    %rsp,%rbp
    subq    $80,%rsp

    movq    %rdi,-8(%rbp)           # *this
    movq    %rsi,-16(%rbp)          # *st1
    movq    %rdx,-32(%rbp)          # st2(lower)
    movq    %rcx,-24(%rbp)          # st2(upper)

    leaq    -72(%rbp),%rdi
    movq    $'S, %rsi
    movq    -16(%rbp),%rbx
    movq    (%rbx), %rdx
    call    _ZN2clC1Ec3st1

    movl    $0,-80(%rbp)

for3:
    movslq  -80(%rbp),%rcx
    cmpq    $4,%rcx
    jl      corpo3
    jmp     fine3
    
corpo3:
    movq    -16(%rbp),%r8           # &s1
    movq    -8(%rbp),%rdx           # this
    movb    (%r8,%rcx),%bl          # s1.vc i
    movb    (%rdx,%rcx),%al         # s.vc
    cmpb    %bl,%al
    jl      then1
    jmp     oltre1

then1:  
    movq    -8(%rbp),%rdx           # this
    movb    -72(%rbp,%rcx),%bl
    movb    %bl,(%rdx,%rcx)

oltre1:
    movq    -8(%rbp),%rdx           # this
    movq     8(%rdx,%rcx,8),%rax    # this.v
    movq    -64(%rbp,%rcx,8),%rbx   # cla.v
    cmpq    %rbx,%rax
    ja      then2
    jmp     continuo3

then2:
    movq    -64(%rbp,%rcx,8),%r8    # cla.v
    addq    %rcx,%r8
    movq    %r8,8(%rdx,%rcx,8)

continuo3:
    incl    -80(%rbp)
    jmp     for3

fine3:
    movq    -8(%rbp),%rax
    leave 
    ret

