# Copyright 2018 Giuseppe Fabiano
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.text

# Record di attivazione
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2clC1EcR3st2
_ZN2clC1EcR3st2:
    pushq   %rbp
    movq    %rsp, %rbp

    # %rdi riservato per *this
    # %sil riservato per c
    # %rdx riservato per *s2

    movsbl  %sil, %esi

    xor     %rcx, %rcx
for1:
    cmpq    $4, %rcx
    jge     endfor1

    movl    %esi, %eax                  # c
    addb    %cl, %al                    # c + 1
    movb    %al, (%rdi, %rcx)           # s.vc[i] = c + 1

    addl    (%rdx, %rcx, 4), %eax       # s2.vd[i] + s.vc[i]
    movslq  %eax, %rax
    movq    %rax, 8(%rdi, %rcx, 8)      # v[i] = s2.vd[i] + s.vc[i];

    incq    %rcx
    jmp for1
endfor1:
    movq    %rdi, %rax
    leave
    ret


# Record di attivazione
#            |     s      # -96
#           v[0]          # -88
#           v[1]          # -80
#           v[2]          # -72
#           v[3]          # -64
#   s2.vd[1] |  s2.vd[0]  # -48
#   s2.vd[3] |  s2.vd[2]  # -40
#            |     s1     # -16
#          *this          #  -8
#        old %rbp         #  -0  <- %rbp
#          %rip           #
.global _ZN2cl5elab1E3st13st2
_ZN2cl5elab1E3st13st2:
    pushq   %rbp
    movq    %rsp, %rbp
    addq    $-96, %rsp                  # allineamento a 16byte

    movq    %rdi, -8(%rbp)              # *this
    movq    %rsi, -16(%rbp)             # s1
    movq    %rdx, -48(%rbp)             # s2 (lower)
    movq    %rcx, -40(%rbp)             # s2 (upper)

    # -96 : cla

    leaq    -96(%rbp), %rdi             # cla address
    movq    $'a,  %rsi
    leaq    -48(%rbp), %rdx
    call    _ZN2clC1EcR3st2

    xor     %rcx, %rcx
for2:
    cmpq    $4, %rcx
    jge     endfor2

    movq    -8(%rbp), %rdi
    movb    (%rdi, %rcx), %al           # s.vc[i]
    cmpb    -16(%rbp, %rcx), %al        # s.vc[i] <= s1.vc[i]
    jg      endif

    movsbl  -96(%rbp, %rcx), %eax       # cla.s.vc[i]
    addl    %ecx, %eax                  # i + cla.s.vc[i]
    movb    %al, (%rdi, %rcx)           # s.vc[i] = i + cla.s.vc[i];

    movq    -96+8(%rbp, %rcx, 8), %rax  # cla.v[i]
    neg     %rax                        # -cla.v[i]
    addq    %rcx, %rax                  # i - cla.v[i]

    movq    %rax, 8(%rdi, %rcx, 8)      # v[i] = i - cla.v[i];
endif:
    incq    %rcx
    jmp     for2
endfor2:
    leave
    ret
