# author: Gianluca Cometa

.global _ZN2clC1EcR3st2
_ZN2clC1EcR3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $32, %rsp

.set this, -8
.set c, -16
.set s2, -24
.set i, -32

# copio parametri
movq %rdi, this(%rbp)
movb %sil, c(%rbp)
movq %rdx, s2(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movb c(%rbp), %al
    addb %cl, %al
    movb %al, (%rdi, %rcx)

    movb (%rdi, %rcx), %bl
    movq s2(%rbp), %r8
    movl (%r8, %rcx, 4), %eax
    movsbl %bl, %ebx
    addl %ebx, %eax
    movslq %eax, %rax
    movq %rax, 8(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1E3st13st2
_ZN2cl5elab1E3st13st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $72, %rsp

.set this, -8
.set s1, -16
.set i, -12
.set s2, -32
.set cla, -72

# copio parametri
movq %rdi, this(%rbp)
movl %esi, s1(%rbp)
movq %rdx, s2(%rbp)
movq %rcx, s2+8(%rbp)

# istanzio cla
leaq cla(%rbp), %rdi
movb $'a', %sil
leaq s2(%rbp), %rdx
call _ZN2clC1EcR3st2

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)    
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

if1:
    movb s1(%rbp, %rcx), %al
    movb (%rdi, %rcx), %bl
    cmpb %al, %bl
    ja fineif1
    movb cla(%rbp, %rcx, 1), %al
    addb %cl, %al
    movb %al, (%rdi, %rcx)

    movq cla+8(%rbp, %rcx, 8), %r8
    movq %rcx, %r9
    subq %r8, %r9
    movq %r9, 8(%rdi, %rcx, 8)
fineif1:
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret
