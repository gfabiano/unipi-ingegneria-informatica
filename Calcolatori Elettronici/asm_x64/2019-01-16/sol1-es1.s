# author: Gianluca Cometa

.global _ZN2clC1EPKc3st2
_ZN2clC1EPKc3st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $40, %rsp

.set this, -8
.set c, -16
.set s2, -32
.set i, -40

# copio parametri
movq %rdi, this(%rbp)
movq %rsi, c(%rbp)
movq %rdx, s2(%rbp)
movq %rcx, s2+8(%rbp)

movl $0, i(%rbp)

for1:
    cmpl $4, i(%rbp)
    jge finefor1
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

    movq c(%rbp), %r8
    movb (%r8, %rcx), %al
    movb %al, 0(%rdi, %rcx, 1)

    movb 0(%rdi, %rcx, 1), %bl
    movl s2(%rbp,%rcx,4), %edx
    movsbl %bl, %ebx
    addl %edx, %ebx
    movslq %ebx, %rbx
    movq %rbx, 8(%rdi, %rcx, 8)

    incl i(%rbp)
    jmp for1
finefor1:
    leave
    ret

.global _ZN2cl5elab1E3st13st2
_ZN2cl5elab1E3st13st2:

# prologo
pushq %rbp
movq %rsp, %rbp
subq $72, %rsp

.set this, -8
.set s1, -16
.set i, -12
.set s2, -32
.set cla, -72

# copio parametri
movq %rdi, this(%rbp)
movl %esi, s1(%rbp)
movq %rdx, s2(%rbp)
movq %rcx, s2+8(%rbp)

# istanzio cla
leaq cla(%rbp), %rdi
leaq s1(%rbp), %rsi
movq s2(%rbp), %rdx
movq s2+8(%rbp), %rcx
call _ZN2clC1EPKc3st2

movl $0, i(%rbp)

for2:
    cmpl $4, i(%rbp)
    jge finefor2
    movslq i(%rbp), %rcx
    movq this(%rbp), %rdi

if1:
    movb s1(%rbp, %rcx), %al
    movb (%rdi, %rcx), %bl
    cmpb %al, %bl
    ja if2
    movb cla(%rbp, %rcx), %al
    movb %al, (%rdi, %rcx)
if2:
    movq cla+8(%rbp, %rcx, 8), %r8
    movq 8(%rdi, %rcx, 8), %r9
    cmpq %r8, %r9
    ja fineif2
    addq %r8, 8(%rdi, %rcx, 8)
fineif2:
    incl i(%rbp)
    jmp for2
finefor2:
    leave
    ret

